# This code needs helpers.py and histo_defs.py which are stored in the same folder as this file
# Following cuts on events are assumed
# jet1.PT > 0 and np.abs(jet1.Eta) < 2.5 and jet2.PT > 500 and np.abs(jet2.Eta) < 2.5
# We assume that different branches corresponding to different jet radii are defined in root files
#
# command: python /path_of_code/transverse_mass.py /path_of_rootfile/name_of_rootfile.root /path_of_rootfile/output_name.root
# Takes the rootfile as input and computes the defined variables and fills the respective histograms.

import sys
import numpy as np
import ROOT
from helpers import *

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 3:
  print(" Usage: Analysis_code/Jet_analysis.py /path/delphes_file.root /path/output.root")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
        ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
        ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
        pass

# Parameters
############################################
# Radius of jets (0.4, 1.0, 1.4) :
# Events selection (pT in GeV)
# pT_min_jet1 = 500
# pT_min_jet2 = 500
pT_min_jet1 = 0
pT_min_jet2 = 0
eta_max = 2.5
M_PI = 3.14
############################################

inputFile = sys.argv[1]
print("Input file :")
print(inputFile)

outputFile = sys.argv[2]
print("output file :")
print(outputFile)

R = float(sys.argv[3])
print(" R used = {}".format(R))
print(type(R))

# Create chain of root trees
chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

# Get pointer to branches used in this analysis
# R-jet branches : 04, 08, 10, 12, 14
R_jet = str(int(R*10))
if R<1.0: R_jet = '0' + R_jet
print("The R value used = {}".format(R_jet))

# Getting the required branches from Delphes ROOT file. Also include the EFlowTracks and EFlowNeutralHadrons to access the Correct TRefArray from the ParticleFlowJet.
branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R_jet)
branchMET = treeReader.UseBranch("MissingET")
branchHT = treeReader.UseBranch("ScalarHT")
branchGenJet = treeReader.UseBranch("GenJet%s"%R_jet)
branchParticle = treeReader.UseBranch("Particle")
branchElectron = treeReader.UseBranch("Electron")
branchPhoton = treeReader.UseBranch("Photon")
branchMuon = treeReader.UseBranch("Muon")
branchTower = treeReader.UseBranch("Tower")
branchtrack = treeReader.UseBranch("Track")
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")
# branchfilteredParticle = treeReader.UseBranch("filteredParticles")


# Book histograms
Nbins = 150
hist1Jet1PT = ROOT.TH1F("jet1_pt", ";p_{T} (j_{1}) GeV;Events", Nbins, 0.0, 800.0)
hist1Jet2PT = ROOT.TH1F("jet2_pt", ";p_{T} (j_{2}) GeV;Events", Nbins, 0.0, 800.0)
histMET = ROOT.TH1F("jet_met", "Missing transverse energy;MET GeV;Events", Nbins, .0, 2000.0)
hist1JetmJJ = ROOT.TH1F("jet_mJJ", " Invariant mass m_{JJ} with R=%.1f;m_{JJ} GeV;Events"%(R), Nbins, 0.0, 2500.0)

Nbins = 40
hist1Jet1Ntrk = ROOT.TH1F("jet1_ntrk", "Lead jet N_{trk} with R=%.1f;N_{trk};Events"%(R), Nbins, .0, 120.0)
# hist2D_ptd_ntrk_j1 = ROOT.TH2F("ptd_vs_ntrk","PTD vs Ntrack with with R=%.1f;p_{T}^{D};Ntracks"%(R),50,0.0,1.0,40,0,50)

Nbins = 12 
hnjets  = ROOT.TH1F("njets",";N_{jets};Events",Nbins, 0, 12)

Nbins = 100
hdeltaR = ROOT.TH1F("deltaR","Delta R between jets and its constituents;#DeltaR;Events",Nbins, 0,R+0.2)
hgendeltaR = ROOT.TH1F("gendeltaR","Delta R between genjets and its constituents;#DeltaR;Events",Nbins, 0,R+0.2)

Nbins = 150
hmt = ROOT.TH1F("jet_met_mt" , "Transverse mass of jet1 + jet2 + MET;#frac{E^{miss}_{T}}{m_{T}};Events" , Nbins, 0.0 ,5000.0)
hmt_jj = ROOT.TH1F("mt_jj",";m_{T}(j_{1}j_{2});Events",Nbins,0.0,3000.0)

Nbins = 50
hdphi1 = ROOT.TH1F("dPhi1", ";#Delta#phi_{j_{1},MET};Events", Nbins, 0.0, 3.4)
hdphi2 = ROOT.TH1F("dPhi2", ";#Delta#phi_{j_{2},MET};Events", Nbins, 0.0, 3.4)
hj1phi = ROOT.TH1F("j1phi", "Leading jet phi;#phi;Events",Nbins,-3.3,3.3)
hj2phi = ROOT.TH1F("j2phi", "Sub Leading jet phi;#phi;Events",Nbins,-3.3,3.3)

Nbins = 100
hrt = ROOT.TH1F("jet_met_rt" , ";R_{T} = #frac{MET}{M_{T}};Events" , Nbins, 0.0 , 0.6)
hht = ROOT.TH1F("HT", "Scalar HT;HT;Events", Nbins, 0.0,4000.0)


Nbins = 150
hist2Jet1PT = ROOT.TH1F("jet1_pt_softdrop", "Lead jet p_{T} with R=%.1f;p_{T}^{SD} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist2Jet2PT = ROOT.TH1F("jet2_pt_softdrop", "Sub jet p_{T} with R=%.1f;p_{T}^{SD} GeV;Events"%(R), Nbins, 0.0, 2000.0)
histAllJetSDPT = ROOT.TH1F("all_jet_pt_softdrop", "All jet p_{T} with R=%.1f;p_{T}^{SD} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist2JetmJJ = ROOT.TH1F("jet_mJJ_softdrop", " Invariant mass m_{JJ} with R=%.1f;m_{JJ}^{SD} GeV;Events"%(R), Nbins, 0.0, 2500.0)
hist2Jet1Mass = ROOT.TH1F("jet1_mass_softdrop", ";m^{SD}_{j_{1}} [GeV];Events", Nbins, 0.0, 500.0)
hist2Jet2Mass = ROOT.TH1F("jet2_mass_softdrop", ";m^{SD}_{j_{2}} [GeV];Events", Nbins, 0.0, 500.0)
# histAllJetSDMass = ROOT.TH1F("all_jet_mass_softdrop", ";m^{SD}_{j_{all}} [GeV];Events", Nbins, 0.0, 500.0)

# hist3Jet1PT = ROOT.TH1F("jet1_pt_trimmed", "Lead jet p_{T} with R=%.1f;p_{T} GeV;Events"%(R), Nbins, 0.0, 2000.0)
# hist3Jet2PT = ROOT.TH1F("jet2_pt_trimmed", "Sub jet p_{T} with R=%.1f;p_{T} GeV;Events"%(R), Nbins, 0.0, 2000.0)
# hist3JetmJJ = ROOT.TH1F("jet_mJJ_trimmed", " Invariant mass m_{JJ} with R=%.1f;m_{JJ} GeV;Events"%(R), Nbins, 0.0, 3500.0)
# histtrackpt = ROOT.TH2F("track_pt", "track PT vs Ntrk1;p_{T};Events", 20,0.0,100.0 , 40,0.0,160.0)

Nbins = 50
histdelEta = ROOT.TH1F("delta_eta", "delta_eta_jet1_and_jet2;#Delta#eta(j_{1},j_{2});Events", Nbins,0.0,4.0)
hj1eta = ROOT.TH1F("j1eta", "Leading jet eta;#eta(j_{1});Events",Nbins,0.0,4.0)
hj2eta = ROOT.TH1F("j2eta", "Sub Leading jet eta;#eta(j_{2});Events",Nbins,0.0,4.0)


Nbins = 100
hmomentgirth_1 = ROOT.TH1F("momentgirth_j1",";girth(j_{1});Events",Nbins,0.0, R)
hmomentgirth_2 = ROOT.TH1F("momentgirth_j2",";girth(j_{2});Events",Nbins,0.0, R)
hmomentgirth_j_all = ROOT.TH1F("momentgirth_j_all",";girth(Jet);Events",Nbins,0.0, R)
hmomenthalf_1  = ROOT.TH1F("momenthalf_j1", ";Moment half (j_{1});Events",Nbins,0.0,R) 
hmomenthalf_2  = ROOT.TH1F("momenthalf_j2", ";Moment half (j_{2});Events",Nbins,0.0,R)
hmomenthalf_j_all  = ROOT.TH1F("momenthalf_j_all", ";Moment half (Jet);Events",Nbins,0.0,R) 
hnpart_j1      = ROOT.TH1F("multiplicity_j1",";Multiplicity(j_{1});Events",Nbins,0,100)
hnpart_j2      = ROOT.TH1F("multiplicity_j2",";Multiplicity(j_{2});Events",Nbins,0,100)
hnpart_j_all      = ROOT.TH1F("multiplicity_j_all",";Multiplicity(Jet);Events",Nbins,0,100)
hptd_j1        = ROOT.TH1F("ptd_j1",";p_{T}^{D}(j_{1});Events",Nbins,0,1.0)
hptd_j2        = ROOT.TH1F("ptd_j2",";p_{T}^{D}(j_{2});Events",Nbins,0,1.0)
hptd_j_all     = ROOT.TH1F("ptd_j_all",";p_{T}^{D}(Jet);Events",Nbins,0,1.0)

hptd_j1_calc        = ROOT.TH1F("ptd_j1_calc","Calculated Leading Jet PtD;p_{T}^{D}(j_{1});Events",Nbins,0,1.0)
hptd_j2_calc        = ROOT.TH1F("ptd_j2_calc","Calculated Sub Leading Jet PtD;p_{T}^{D}(j_{2});Events",Nbins,0,1.0)
hptd_j_all_calc     = ROOT.TH1F("ptd_j_all_calc","Calculated All Leading Jet PtD;p_{T}^{D}(Jet);Events",Nbins,0,1.0)

hmajaxis_j1    = ROOT.TH1F("majaxis_j1",";#sigma_{major}(j_{1});Events",Nbins,0,R)
hmajaxis_j2    = ROOT.TH1F("majaxis_j2",";#sigma_{major}(j_{2});Events",Nbins,0,R)
hmajaxis_j_all    = ROOT.TH1F("majaxis_j_all",";#sigma_{major}(Jet);Events",Nbins,0,R)
hminaxis_j1    = ROOT.TH1F("minaxis_j1",";#sigma_{minor}(j_{1});Events",Nbins,0,R)
hminaxis_j2    = ROOT.TH1F("minaxis_j2",";#sigma_{minor}(j_{2});Events",Nbins,0,R)
hminaxis_j_all    = ROOT.TH1F("minaxis_j_all",";#sigma_{minor}(Jet);Events",Nbins,0,R)
havgaxis_j1    = ROOT.TH1F("avgaxis_j1",";#sigma_{avg}(j_{1});Events",Nbins,0,R)
havgaxis_j2    = ROOT.TH1F("avgaxis_j2",";#sigma_{avg}(j_{2});Events",Nbins,0,R)
havgaxis_j_all    = ROOT.TH1F("avgaxis_j_all",";#sigma_{avg}(Jet);Events",Nbins,0,R)
htau1_j1       = ROOT.TH1F("tau1_j1","Leading Jet #tau_1;#tau_{1};Events",Nbins,0,1.0)
htau1_j2       = ROOT.TH1F("tau1_j2","Sub Leading Jet #tau_{1};#tau_{1};Events",Nbins,0,1.0)
htau2_j1       = ROOT.TH1F("tau2_j1","Leading Jet #tau_{2};#tau_{2};Events",Nbins,0,1.0)
htau2_j2       = ROOT.TH1F("tau2_j2","Sub Leading Jet #tau_{2};#tau_{2};Events",Nbins,0,1.0)
htau3_j1       = ROOT.TH1F("tau3_j1","Leading Jet #tau_{3};#tau_{3};Events",Nbins,0,1.0)
htau3_j2       = ROOT.TH1F("tau3_j2","Sub Leading Jet #tau_{3};#tau_{3};Events",Nbins,0,1.0)
htau21_j1      = ROOT.TH1F("tau21_j1",";#tau_{21}(j_{1});Events",Nbins,0,1.0)
htau21_j2      = ROOT.TH1F("tau21_j2",";#tau_{21}(j_{2});Events",Nbins,0,1.0)
htau21_j_all   = ROOT.TH1F("tau21_j_all",";#tau_{21}(Jet);Events",Nbins,0,1.0)
htau32_j1      = ROOT.TH1F("tau32_j1",";#tau_{32}(j_{1});Events",Nbins,0,1.0)
htau32_j2      = ROOT.TH1F("tau32_j2",";#tau_{32}(j_{2});Events",Nbins,0,1.0)
htau32_j_all     = ROOT.TH1F("tau32_j_all",";#tau_{32}(Jet);Events",Nbins,0,1.0)
hlha_j1        = ROOT.TH1F("lha_j1",";LHA(j_{1});Events",Nbins,0,1.5)
hlha_j2        = ROOT.TH1F("lha_j2",";LHA(j_{2});Events",Nbins,0,1.5)
hlha_j_all        = ROOT.TH1F("lha_j_all",";LHA(Jet);Events",Nbins,0,1.5)
hthrust_j1     = ROOT.TH1F("thrust_j1",";Thrust(j_{1});Events",Nbins,0,1.0)
hthrust_j2     = ROOT.TH1F("thrust_j2",";Thrust(j_{2});Events",Nbins,0,1.0)
hthrust_j_all     = ROOT.TH1F("thrust_j_all",";Thrust(Jet);Events",Nbins,0,1.0)

## debugging histograms
Nbins = 150
deb_hpart_pt   = ROOT.TH1F("deb_hjet_pt","Debug Plot: Part pt with variable of interest is zero;p^{T};Events",Nbins, 0.0, 800.0)
deb_hpart_dR   = ROOT.TH1F("deb_hpart_dR","Debug Plot: DR(constituent,jet) with variable of interest is zero;#DeltaR(i,jet);Events",Nbins,0.0,R)
deb_hpart_dR_pt= ROOT.TH1F("deb_hjet_dR_pt","Debug Plot: Part pt*DR(constituent,jet) with variable of interest is zero;p^{T};Events",Nbins, 0.0, 1.0)

# hfrac_pt       = ROOT.TH1F("fracpt","Frac pt;")
# TODO add more variables like Energy fraction stuff. 

counter_j1 = 0 


# Loop over all events
for entry in range(0, numberOfEntries):
    # Load selected branches with data from specified event
    treeReader.ReadEntry(entry)
    if (entry+1)%10000 == 0:
       print ("... processed {} events ...".format(entry+1)) 
    njets = branchJet.GetEntries()
    
    hnjets.Fill(njets) # Fill the njets histogram
    
    # Cheching the DeltaR values between the mapped constituents in the Jet.Constitutents 
    # for i in range(0,branchJet.GetEntries()):
    #   for constituents in branchJet.At(i).Constituents:
    #     tempjet = branchJet.At(i)
    #     deta = GetdEta(constituents.Eta,tempjet.Eta)
    #     dphi = GetdPhi(constituents.Phi,tempjet.Phi)
    #     dR = GetdR(deta,dphi)
    #     hdeltaR.Fill(dR)
        # if dR < 0.4 : hdeltaR.Fill(dR)
    # Checking the gen DeltaR values between the mapped constituents in the GenJet.Constituents 
    for i in range(0,branchGenJet.GetEntries()):
      for constituent in branchGenJet.At(i).Constituents:
        tempgjet = branchGenJet.At(i)
        gdeta = GetdEta(constituent.Eta,tempgjet.Eta)
        gdphi = GetdPhi(constituent.Phi,tempgjet.Phi)
        gdR = GetdR(gdeta,gdphi)
        hgendeltaR.Fill(gdR)

    # If event contains at least 1 jet
    if branchJet.GetEntries() > 0:
      jet1 = branchJet.At(0)
      # print("Number of jets = {}".format(branchJet.GetEntries()))
      # if jet1.PTD == 1 or jet2.PTD == 1:
      #   continue
        
      #Selecting events
      if np.abs(jet1.Eta) < eta_max: 
        # Defining the TLorentz vector for leading jet
        vec1 = GetJetVector(jet1.PT , jet1.Eta , jet1.Phi , jet1.Mass)

        # Defining TLorentz vector for missing energy branch
        met = branchMET.At(0)
        m = met.MET
        METPhi = met.Phi
        METx = m* np.cos(METPhi)
        METy = m* np.sin(METPhi)
        vecmet = ROOT.TLorentzVector()
        vecmet.SetPxPyPzE(METx , METy , 0 , m)
        htbranch = branchHT.At(0)
        ht = htbranch.HT

        # Computing dPhi between jet1 and met
        dPhi1 = GetdPhi(jet1.Phi, met.Phi)

        # SoftDropped jets
        # Getting the Soft dropped jet1 four momenta
        jet1_softdrop = jet1.SoftDroppedP4[0]
        jet1_pt_softdrop = jet1_softdrop.Pt()
        jet1_mass_sofdrop = jet1_softdrop.M()
        

        # Working on second leading jet
        if branchJet.GetEntries() > 1:
          jet2 = branchJet.At(1)
          vec2 = GetJetVector(jet2.PT , jet2.Eta , jet2.Phi , jet2.Mass)
          # Computing transverse mass for jet1+jet2+met
          mt = (vec1 + vec2 + vecmet).Mt()
          # Computing transverse mass for jet1 and jet2
          mt_jj = (vec1 + vec2).Mt()
          if (mt!=0): rt = m/mt
          dPhi2 = GetdPhi(jet2.Phi, met.Phi)
          jet2_softdrop = jet2.SoftDroppedP4[0]
          jet2_pt_softdrop = jet2_softdrop.Pt()
          jet2_mass_sofdrop = jet2_softdrop.M()   
          invmass_softdrop = Getinvmass(jet1_softdrop , jet2_softdrop)      
          hist1Jet2PT.Fill(jet2.PT)
          hist1JetmJJ.Fill((jet1.P4() + jet2.P4()).M())
          hmt.Fill(mt)
          hmt_jj.Fill(mt_jj)
          hrt.Fill(rt)
          hist2JetmJJ.Fill(invmass_softdrop)
          hist1Jet2PT.Fill(jet2_pt_softdrop)
          hist2Jet2Mass.Fill(jet1_mass_sofdrop)
          delEta = GetdEta(jet1.Eta , jet2.Eta)
          histdelEta.Fill(delEta)
          hj2eta.Fill(jet2.Eta)
          hj2phi.Fill(jet2.Phi)
          

        
        # Trimmed jets
        # # Getting the Trimmed jet1 four momenta
        # jet1_trimmed = jet1.TrimmedP4[0]
        # jet1_pt_trimmed = jet1_trimmed.Pt()

        # # Trimmed jets
        # # Getting the Trimmed jet1 four momenta
        # jet1_trimmed = jet1.TrimmedP4[0]
        # jet1_pt_trimmed = jet1_trimmed.Pt()
        
        # looping over the jet and calculating the jet substructure variables
        for i in range(0,branchJet.GetEntries()):
          jet = branchJet.At(i)
          if i == 0:
            hist1Jet1Ntrk.Fill(jet.NCharged)
          # filling the Tau & PTD for jet 1
          if i == 0: 
            htau1_j1.Fill(jet.Tau[0])
            htau2_j1.Fill(jet.Tau[1])
            htau3_j1.Fill(jet.Tau[2])
            if jet.Tau[1] != 0:
              htau21_j1.Fill(jet.Tau[1]/jet.Tau[0])
            if jet.Tau[2] != 0:
              htau32_j1.Fill(jet.Tau[2]/jet.Tau[1]) 
            hptd_j1.Fill(jet.PTD)

          # filling the Tau & PTD for jet 2
          if i == 1:
            htau1_j2.Fill(jet.Tau[0])
            htau2_j2.Fill(jet.Tau[1])
            htau3_j2.Fill(jet.Tau[2])
            if jet.Tau[1] != 0:
              htau21_j2.Fill(jet.Tau[1]/jet.Tau[0])
            if jet.Tau[2] != 0:
              htau32_j2.Fill(jet.Tau[2]/jet.Tau[1])
            hptd_j2.Fill(jet.PTD)

          # filling the Tau for all jets
          if jet.Tau[1] != 0:
            htau21_j_all.Fill(jet.Tau[1]/jet.Tau[0])
          if jet.Tau[2] != 0:
            htau32_j_all.Fill(jet.Tau[2]/jet.Tau[1])  
          # filling ptd for all jets  
          hptd_j_all.Fill(jet.PTD)
          
          # Initializing jet substructure variables of interest that will be calculated by looping over the particles
          momentGirth, momentHalf, sumpt, sumpt2, sumdeta, sumdphi, sumdeta2, sumdphi2, sumdetadphi = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
          lha, thrust = 0.0, 0.0
          ptd, axis1, axis2, axisA = 0.0,0.0,0.0,0.0
          no_of_particles = jet.Constituents.GetEntries()
          if i == 0:
            hnpart_j1.Fill(no_of_particles)
          if i == 1:
            hnpart_j2.Fill(no_of_particles)

          hnpart_j_all.Fill(no_of_particles)

          # Now looping over all the constituents
          for particle in jet.Constituents:
            deta = GetdEta(particle.Eta, jet.Eta)
            dphi = GetdPhi(particle.Phi, jet.Phi)    
            dR = GetdR(deta, dphi)
            hdeltaR.Fill(dR)
            if particle.ClassName() == 'Tower':
              pT = particle.ET
            else:
              pT = particle.PT
            pt2 = pT**2
            momentGirth += pT*dR 
            momentHalf += pT*ROOT.TMath.Sqrt(dR)
            lha += pT*ROOT.TMath.Sqrt(dR/R)
            thrust += pT*(dR**2)/R**2
            sumpt += pT
            sumpt2 += pt2
            sumdeta += deta*pt2
            sumdphi += dphi*pt2
            sumdeta2 += deta*deta*pt2
            sumdphi2 += dphi*dphi*pt2
            sumdetadphi += deta*dphi*pt2

          if sumpt2 != 0:
            sumdeta /= sumpt2
            sumdphi /= sumpt2
            sumdeta2 /= sumpt2
            sumdphi2 /= sumpt2
            sumdetadphi /= sumpt2

            a = sumdeta2 - sumdeta*sumdeta
            b = sumdphi2 - sumdphi*sumdphi
            c = sumdeta*sumdphi - sumdetadphi
            ptd_1_calc = ROOT.TMath.Sqrt(sumpt2)/sumpt
            d = np.sqrt(np.fabs((a-b)*(a-b)+4*c*c))
            axis1 = ROOT.TMath.Sqrt(0.5*(a+b+d)) if (a+b+d)>0 else 0.0
            axis2 = ROOT.TMath.Sqrt(0.5*(a+b-d)) if (a+b-d)>0 else 0.0
            axisA = ROOT.TMath.Sqrt(axis1*axis1 + axis2*axis2)
            momentGirth /= jet.PT
            momentHalf /= jet.PT
            lha /= jet.PT
            thrust /= jet.PT
            # filling for the jet 1
            if i == 0:
              hmomentgirth_1.Fill(momentGirth)
              hmomenthalf_1.Fill(momentHalf)
              hptd_j1_calc.Fill(ptd_1_calc)
              hmajaxis_j1.Fill(axis1)
              hminaxis_j1.Fill(axis2)
              havgaxis_j1.Fill(axisA)
              hlha_j1.Fill(lha)
              hthrust_j1.Fill(thrust)
              # filling for the jet 2
            if i == 1:
              hmomentgirth_2.Fill(momentGirth)
              hmomenthalf_2.Fill(momentHalf)
              hptd_j2_calc.Fill(ptd_1_calc)
              hmajaxis_j2.Fill(axis1)
              hminaxis_j2.Fill(axis2)
              havgaxis_j2.Fill(axisA)
              hlha_j2.Fill(lha)
              hthrust_j2.Fill(thrust)
            # filling all the jets
            hmomentgirth_j_all.Fill(momentGirth)
            hmomenthalf_j_all.Fill(momentHalf)
            hptd_j_all_calc.Fill(ptd_1_calc)
            hmajaxis_j_all.Fill(axis1)
            hminaxis_j_all.Fill(axis2)
            havgaxis_j_all.Fill(axisA)
            hlha_j_all.Fill(lha)
            hthrust_j_all.Fill(thrust)

          if sumpt2 == 0: 
            counter_j1+=1


        # for i in range(0,branchJet.GetEntries()):
        #   jet = branchJet.At(i)

        #   #Calculating average track transverse momentum
        #   selectedtrack = []
        #   selectedtower = []
        #   #Store the tracks to array called "track"
        #   for k in range(0 , branchtrack.GetEntries()):  
        #     dPhi = GetdPhi(branchtrack.At(k).Phi , jet.Phi)
        #     dEta = GetdEta(branchtrack.At(k).Eta , jet.Eta)
        #     DeltaR = np.sqrt(dEta*dEta + dPhi*dPhi)
        #       #Store the tracks which satisfy the condition on delta R to array "selectedtrack"
        #     if DeltaR < R:
        #       selectedtrack.append(branchtrack.At(k))
          
        #   for l in range(0,branchTower.GetEntries()):
        #     dPhi = GetdPhi(branchTower.At(l).Phi, jet.Phi)
        #     dEta = GetdEta(branchTower.At(l).Eta, jet.Eta)
        #     DeltaR = GetdR(dPhi,dEta)
        #     if DeltaR < R:  
        #       selectedtower.append(branchTower.At(l))

        #   constit = selectedtower + selectedtrack
        #   # Case 1 : running without adding towers to the jet
        #   # constit = selectedtower

        #   # case 2 : running with condition of having two tracks
        #   if len(selectedtrack) < 2 : continue

        #   if i == 0:
        #     hist1Jet1Ntrk.Fill(len(selectedtrack))
        #   # filling the Tau & PTD for jet 1
        #   if i == 0: 
        #     htau1_j1.Fill(jet.Tau[0])
        #     htau2_j1.Fill(jet.Tau[1])
        #     htau3_j1.Fill(jet.Tau[2])
        #     if jet.Tau[1] != 0:
        #       htau21_j1.Fill(jet.Tau[1]/jet.Tau[0])
        #     if jet.Tau[2] != 0:
        #       htau32_j1.Fill(jet.Tau[2]/jet.Tau[1]) 
        #     hptd_j1.Fill(jet.PTD)

        #   # filling the Tau & PTD for jet 2
        #   if i == 1:
        #     htau1_j2.Fill(jet.Tau[0])
        #     htau2_j2.Fill(jet.Tau[1])
        #     htau3_j2.Fill(jet.Tau[2])
        #     if jet.Tau[1] != 0:
        #       htau21_j2.Fill(jet.Tau[1]/jet.Tau[0])
        #     if jet.Tau[2] != 0:
        #       htau32_j2.Fill(jet.Tau[2]/jet.Tau[1])
        #     hptd_j2.Fill(jet.PTD)

        #   # filling the Tau for all jets
        #   if jet.Tau[1] != 0:
        #     htau21_j_all.Fill(jet.Tau[1]/jet.Tau[0])
        #   if jet.Tau[2] != 0:
        #     htau32_j_all.Fill(jet.Tau[2]/jet.Tau[1])  
        #   # filling ptd for all jets  
        #   hptd_j_all.Fill(jet.PTD)


          # momentGirth, momentHalf, sumpt, sumpt2, sumdeta, sumdphi, sumdeta2, sumdphi2, sumdetadphi = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
          # lha, thrust = 0.0, 0.0
          # no_of_particles = len(constit)
          # # filling the number of particles
          # if i == 0:
          #   hnpart_j1.Fill(no_of_particles)
          # if i == 1:
          #   hnpart_j2.Fill(no_of_particles)

          # hnpart_j_all.Fill(no_of_particles)

          # ptd, axis1, axis2, axisA = 0.0,0.0,0.0,0.0
          # # looping over the constituents of the jet and calculating the jet substructure variables
          # for particle in constit:
          #   deta = GetdEta(particle.Eta, jet.Eta)
          #   dphi = GetdPhi(particle.Phi, jet.Phi)    
          #   dR = GetdR(deta, dphi)
          #   # print("type = {}".format(particle.ClassName()))

          #   # checking if the particle is of class Tower or Track
          #   if particle.ClassName() == 'Tower':
          #     pT = particle.ET
          #   else:
          #     pT = particle.PT

          #   if dR < R: 
          #     pt2 = pT**2
          #     momentGirth += pT*dR 
          #     momentHalf += pT*ROOT.TMath.Sqrt(dR)
          #     lha += pT*ROOT.TMath.Sqrt(dR/R)
          #     thrust += pT*(dR**2)/R**2
          #     sumpt += pT
          #     sumpt2 += pt2
          #     sumdeta += deta*pt2
          #     sumdphi += dphi*pt2
          #     sumdeta2 += deta*deta*pt2
          #     sumdphi2 += dphi*dphi*pt2
          #     sumdetadphi += deta*dphi*pt2

          # if sumpt2 != 0:
          #   sumdeta /= sumpt2
          #   sumdphi /= sumpt2
          #   sumdeta2 /= sumpt2
          #   sumdphi2 /= sumpt2
          #   sumdetadphi /= sumpt2
            
          #   a = sumdeta2 - sumdeta*sumdeta
          #   b = sumdphi2 - sumdphi*sumdphi
          #   c = sumdeta*sumdphi - sumdetadphi
          #   ptd_1_calc = ROOT.TMath.Sqrt(sumpt2)/sumpt
          #   d = np.sqrt(np.fabs((a-b)*(a-b)+4*c*c))
          #   axis1 = ROOT.TMath.Sqrt(0.5*(a+b+d)) if (a+b+d)>0 else 0.0
          #   axis2 = ROOT.TMath.Sqrt(0.5*(a+b-d)) if (a+b-d)>0 else 0.0
          #   axisA = ROOT.TMath.Sqrt(axis1*axis1 + axis2*axis2)
          #   momentGirth /= jet.PT
          #   momentHalf /= jet.PT
          #   lha /= jet.PT
          #   thrust /= jet.PT
          #   # filling for the jet 1
          #   if i == 0:
          #     hmomentgirth_1.Fill(momentGirth)
          #     hmomenthalf_1.Fill(momentHalf)
          #     hptd_j1_calc.Fill(ptd_1_calc)
          #     hmajaxis_j1.Fill(axis1)
          #     hminaxis_j1.Fill(axis2)
          #     havgaxis_j1.Fill(axisA)
          #     hlha_j1.Fill(lha)
          #     hthrust_j1.Fill(thrust)
          #     # filling for the jet 2
          #   if i == 1:
          #     hmomentgirth_2.Fill(momentGirth)
          #     hmomenthalf_2.Fill(momentHalf)
          #     hptd_j2_calc.Fill(ptd_1_calc)
          #     hmajaxis_j2.Fill(axis1)
          #     hminaxis_j2.Fill(axis2)
          #     havgaxis_j2.Fill(axisA)
          #     hlha_j2.Fill(lha)
          #     hthrust_j2.Fill(thrust)
          #   # filling all the jets
          #   hmomentgirth_j_all.Fill(momentGirth)
          #   hmomenthalf_j_all.Fill(momentHalf)
          #   hptd_j_all_calc.Fill(ptd_1_calc)
          #   hmajaxis_j_all.Fill(axis1)
          #   hminaxis_j_all.Fill(axis2)
          #   havgaxis_j_all.Fill(axisA)
          #   hlha_j_all.Fill(lha)
          #   hthrust_j_all.Fill(thrust)

          # if sumpt2 == 0: 
          #   counter_j1+=1
            
          # For debugging --  
          if momentGirth == 0 or momentHalf == 0 or lha == 0 or axis1 == 0 or axis2 == 0:
            # print("momentGirth = {}, momentHalf == {}, lha = {}, axis major = {}, axis minor = {}".format(momentGirth,momentHalf,lha,axis1,axis2))
            # print("Jet info : PT = {}, eta = {}, Phi = {}, number of particles = {}".format(jet1.PT,jet1.Eta,jet1.Phi,no_of_particles))
            # print("^^^^^^^^^^^^^^^^^^^^^^^^^^     Particle Info  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            for particle in jet.Constituents:
              deta = GetdEta(particle.Eta, jet1.Eta)
              dphi = GetdPhi(particle.Phi, jet1.Phi)    
              dR = GetdR(deta, dphi)
              if particle.ClassName() == 'Tower':
                pt = particle.ET
              else:
                pt = particle.PT
            #   print("type = {}, Constituents info --- PT = {}, Eta = {}, Phi = {},  dphi = {}, deta {}, dR = {}, pT*dR = {}".format(type(particle), pt,particle.Eta,particle.Phi,dphi,deta,dR,dR*pt))
            # print(" +++++++++++++++++++++++++++    end      ++++++++++++++++++++")
            deb_hpart_dR.Fill(dR)
            deb_hpart_pt.Fill(pt)
            deb_hpart_dR_pt.Fill(dR*pt)
          
            
            

          #Fill histograms
          hist1Jet1PT.Fill(jet1.PT)
          
          # hist1Jet1Ntrk.Fill(jet1.NCharged)
          histMET.Fill(branchMET.At(0).MET)
          hdphi1.Fill(dPhi1)
          hist2Jet1PT.Fill(jet1_pt_softdrop)
          hist2Jet1Mass.Fill(jet1_mass_sofdrop)
          hj1eta.Fill(jet1.Eta)
          hj1phi.Fill(jet1.Phi)
          hht.Fill(ht)
          # hist2D_ptd_ntrk_j1.Fill(ptd_1,jet1.NCharged)
          
    
            
#Printing number of accepted events
integral=hist1Jet1PT.Integral(0,-1)
print("integral=",integral)
print("jet 1 with the problem {}".format(counter_j1))
print(numberOfEntries)

#Normalizing the histogram



hist1Jet1PT = Normalizedhist(hist1Jet1PT)
hist1Jet2PT = Normalizedhist(hist1Jet2PT)
histMET = Normalizedhist(histMET)
hist1JetmJJ=Normalizedhist(hist1JetmJJ)
hmt = Normalizedhist(hmt)
hmt_jj = Normalizedhist(hmt_jj)
hdphi1 = Normalizedhist(hdphi1)
hdphi2 = Normalizedhist(hdphi2)
hj1phi = Normalizedhist(hj1phi)
hj2phi = Normalizedhist(hj2phi)
hrt = Normalizedhist(hrt)
hht = Normalizedhist(hht)
hist2Jet1PT = Normalizedhist(hist2Jet1PT)
hist2Jet2PT = Normalizedhist(hist2Jet2PT)
hist2Jet1Mass = Normalizedhist(hist2Jet1Mass)
hist2Jet2Mass = Normalizedhist(hist2Jet2Mass)
hist2JetmJJ = Normalizedhist(hist2JetmJJ)
histdelEta = Normalizedhist(histdelEta)
hj1eta = Normalizedhist(hj1eta)
hj2eta = Normalizedhist(hj2eta)

hmomentgirth_1= Normalizedhist(hmomentgirth_1)
hmomentgirth_2=Normalizedhist(hmomentgirth_2)
hmomentgirth_j_all= Normalizedhist(hmomentgirth_j_all)
hmomenthalf_1 = Normalizedhist(hmomenthalf_1)
hmomenthalf_2 = Normalizedhist(hmomenthalf_2)
hmomenthalf_j_all= Normalizedhist(hmomenthalf_j_all)

hnpart_j1 = Normalizedhist(hnpart_j1)
hnpart_j2 = Normalizedhist(hnpart_j2)
hnpart_j_all=Normalizedhist(hnpart_j_all)

hptd_j1   = Normalizedhist(hptd_j1)
hptd_j2   = Normalizedhist(hptd_j2)
hptd_j_all= Normalizedhist(hptd_j_all)
hptd_j1_calc   = Normalizedhist(hptd_j1_calc)
hptd_j2_calc = Normalizedhist(hptd_j2_calc)
hptd_j_all_calc=Normalizedhist(hptd_j_all_calc)

hmajaxis_j1=Normalizedhist(hmajaxis_j1)
hminaxis_j1=Normalizedhist(hminaxis_j1)
havgaxis_j1=Normalizedhist(havgaxis_j1)
hmajaxis_j2=Normalizedhist(hmajaxis_j2)
hminaxis_j2=Normalizedhist(hminaxis_j2)
havgaxis_j2=Normalizedhist(havgaxis_j2)
hmajaxis_j_all=Normalizedhist(hmajaxis_j_all)
hminaxis_j_all=Normalizedhist(hminaxis_j_all)
havgaxis_j_all=Normalizedhist(havgaxis_j_all)

htau1_j1  = Normalizedhist(htau1_j1)
htau2_j1  = Normalizedhist(htau2_j1)
htau3_j1  = Normalizedhist(htau3_j1)
htau21_j1 = Normalizedhist(htau21_j1)
htau32_j1 = Normalizedhist(htau32_j1)
htau1_j2= Normalizedhist(htau1_j2)
htau2_j2= Normalizedhist(htau2_j2)
htau3_j2= Normalizedhist(htau3_j2)
htau21_j2=Normalizedhist(htau21_j2)
htau32_j2=Normalizedhist(htau32_j2)
htau21_j_all=Normalizedhist(htau21_j_all)
htau32_j_all=Normalizedhist(htau32_j_all)

hnjets = Normalizedhist(hnjets)
hdeltaR = Normalizedhist(hdeltaR)
hgendeltaR = Normalizedhist(hgendeltaR)

hlha_j1 = Normalizedhist(hlha_j1)
hlha_j2 = Normalizedhist(hlha_j2)
hlha_j_all=Normalizedhist(hlha_j_all)

hthrust_j1 = Normalizedhist(hthrust_j1)
hthrust_j2=Normalizedhist(hthrust_j2)
hthrust_j_all=Normalizedhist(hthrust_j_all)
hist1Jet1Ntrk = Normalizedhist(hist1Jet1Ntrk)

# hnjets_after_cuts = Normalizedhist(hnjets_after_cuts)

#Creating a list and saving the histograms to the list.

deb_hpart_dR = Normalizedhist(deb_hpart_dR)
deb_hpart_pt = Normalizedhist(deb_hpart_pt)
deb_hpart_dR_pt=Normalizedhist(deb_hpart_dR_pt)



histlist = ROOT.TList()

histlist.Add(hist1Jet1PT)
histlist.Add(hist1Jet2PT)
histlist.Add(hist1JetmJJ)
# histlist.Add(hist1Jet1Ntrk)
# histlist.Add(hist1Jet2Ntrk)
histlist.Add(histMET)
histlist.Add(hmt)
histlist.Add(hmt_jj)
histlist.Add(hdphi1)
histlist.Add(hdphi2)
histlist.Add(hrt)
histlist.Add(histdelEta)
histlist.Add(hist2Jet1PT)
histlist.Add(hist2JetmJJ)
histlist.Add(hist2Jet1Mass)
histlist.Add(hist2Jet2Mass)
histlist.Add(hist2Jet2PT)

histlist.Add(hj1eta)
histlist.Add(hj2eta)
histlist.Add(hj1phi)
histlist.Add(hj2phi)
histlist.Add(hht)
histlist.Add(hmomentgirth_1)
histlist.Add(hmomentgirth_2)
histlist.Add(hmomentgirth_j_all)
histlist.Add(hmomenthalf_1)
histlist.Add(hmomenthalf_2)
histlist.Add(hmomenthalf_j_all)
histlist.Add(hnpart_j1)
histlist.Add(hnpart_j2)
histlist.Add(hnpart_j_all)
histlist.Add(hptd_j1)
histlist.Add(hptd_j2)
histlist.Add(hptd_j_all)

histlist.Add(hptd_j1_calc)
histlist.Add(hptd_j2_calc)
histlist.Add(hptd_j_all_calc)

histlist.Add(hmajaxis_j1)
histlist.Add(hmajaxis_j2)
histlist.Add(hmajaxis_j_all)
histlist.Add(hminaxis_j1)
histlist.Add(hminaxis_j2)
histlist.Add(hminaxis_j_all)
histlist.Add(havgaxis_j1)
histlist.Add(havgaxis_j2)
histlist.Add(havgaxis_j_all)

histlist.Add(htau1_j1)
histlist.Add(htau2_j1)
histlist.Add(htau3_j1)
histlist.Add(htau21_j1)
histlist.Add(htau32_j1)
histlist.Add(htau1_j2)
histlist.Add(htau2_j2)
histlist.Add(htau21_j2)
histlist.Add(htau32_j2)
histlist.Add(htau21_j_all)
histlist.Add(htau32_j_all)

histlist.Add(hnjets)
# histlist.Add(hist2D_ptd_ntrk_j1)
histlist.Add(hdeltaR)
histlist.Add(hgendeltaR)
# histlist.Add(hgenmajaxis_j1)
# histlist.Add(hgenminaxis_j1)
# histlist.Add(hgenmomentgirth_1)
# histlist.Add(hgenptd_j1_calc)
histlist.Add(hlha_j1)
histlist.Add(hlha_j2)
histlist.Add(hlha_j_all)
histlist.Add(hthrust_j1)
histlist.Add(hthrust_j2)
histlist.Add(hthrust_j_all)
histlist.Add(deb_hpart_pt)
histlist.Add(deb_hpart_dR)
histlist.Add(deb_hpart_dR_pt)

histlist.Add(hist1Jet1Ntrk)
# histlist.Add(hnjets_after_cuts)

#outputFile = inputFile[:-5] + "_combined_R14.root"
rootFile = ROOT.TFile(outputFile, "RECREATE")
histlist.Write()
rootFile.Close()
