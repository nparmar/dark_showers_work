# Authors and contacts:
# Guillaume Albouy: guillaume.albouy@etu.univ-grenoble-alpes.fr
# Akanksha Singh: akki153209@gmail.com
# Harikrishnan Nair: hunair1996@gmail.com

# Following cuts on events are assumed
# jet1.PT > 500 and np.abs(jet1.Eta) < 2.5 and jet2.PT > 500 and np.abs(jet2.Eta) < 2.5
# We assume that different branches corresponding to different jet radii are defined in root files
# In this code jet clustering radius is fixed to 1.4, can be changed at line 77
# This code will analyse input sample and create following reconstructed level normalized distributions:
# 1) pt of leading/subleading jet
# 2) dijet invarint mass
# 3) missing energy
# 4) transverse mass of dijet and met system
# 5) transverse ratio
# 6) delta phi between missin energy and leading/subleading jet
# 7) 2D histo of track pT of leading jet
# 8) delta eta between leading and subleading jet
#
# command: python /path_of_code/transverse_mass.py /path_of_rootfile/name_of_rootfile.root
# Takes the rootfile as input and computes the defined variables and fills the respective histograms.

import sys
import numpy as np
import ROOT
from array import array

M_PI = 3.14
# Define all common procedures here


def GetJetVector(jetpt, jeteta, jetphi, jetmass):
    # Given the jet pt, eta, phi and mass, this will return a TLorentzVector
    px = jetpt*np.cos(jetphi)
    py = jetpt*np.sin(jetphi)
    pz = jetpt*np.sinh(jeteta)
    energy = np.sqrt((jetmass * jetmass) +
                     (jetpt*np.cosh(jeteta) * jetpt*np.cosh(jeteta)))
    JetVector = ROOT.TLorentzVector()
    JetVector.SetPxPyPzE(px, py, pz, energy)
    return JetVector


def GetdPhi(phi1, phi2):
    # Given two phi angles, this returns the difference between them
    dPhi = phi1 - phi2
    if (dPhi > M_PI): dPhi -= 2*M_PI
    if (dPhi <= - M_PI): dPhi += 2*M_PI
    return dPhi


def GetdEta(eta1, eta2):
    # Given two eta values, this will return the difference between them
    dEta = eta1 - eta2
    return dEta


def Getinvmass(obj1, obj2):
    # Given 2 four-momenta, this will return the invariant mass
    invmass = (obj1 + obj2).M()
    return invmass

# My additions to the code


def GetdR(obj1, obj2):
    """Gives a DeltaR given the Delta Eta and Delta Phi"""
    dR = np.sqrt(obj1**2 + obj2**2)
    return dR


def GetJetvariables(jet,R):
    """Returns MomentGirth, MomentHalf, GetMultiplicity,......."""
    momentGirth = 0.0
    momentHalf, sumpt, sumpt2, sumdeta, sumdphi, sumdeta2, sumdphi2, sumdetadphi = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    no_of_particles = 0
    ptd, axis1, axis2, axisA = 0.0,0.0,0.0,0.0
    # print("jet values --  pt = {}, eta = {}, phi = {}".format(jet.PT,jet.Eta,jet.Phi))
    for particle in jet.Constituents:
        # dphi = particle.Phi
        # deta = particle.Eta
        deta = GetdEta(particle.Eta, jet.Eta)
        dphi = GetdPhi(particle.Phi, jet.Phi)
            
        dR = GetdR(deta, dphi)
        if dR < R: 
        # dphi = particle.Phi
        # deta = particle.Eta
        # dR   = GetdR(dphi,deta)
        # print("particle values --  pt = {}, eta = {}, phi = {}".format(particle.PT,particle.Eta,particle.Phi))
        # print("dR = {}, deta = {}, dphi = {}".format(dR,deta,dphi))
            pT = particle.PT
            if pT == 0: print("particle pt is zero")
            pt2 = pT**2
            if pt2 == 0: print("particle pt2 is zero")
            momentGirth += pT*dR 
            if momentGirth == 0: print("momentGirth is zero")
            momentHalf += pT*ROOT.TMath.Sqrt(dR)
            sumpt += pT
            sumpt2 += pt2
            sumdeta += deta*pt2
            sumdphi += dphi*pt2
            sumdeta2 += deta*deta*pt2
            sumdphi2 += dphi*dphi*pt2
            sumdetadphi += deta*dphi*pt2

    # if sumpt2 == 0:  # FIXME Sumpt is calculated to 0 so don't trust the variables right now
    #     # print("sumpt2 is 0, setting it to 1.0, but the jet pt is {}".format(jet.PT))
    #     # sumpt2 = 1.0
    #     # counter += 1 
    # if sumpt == 0:
        # print("sumpt is 0, setting it to 1.0")
        # sumpt = 1.0
    # if sumdeta == 0: 
    #     # print("sum deta is 0")
    a, b, c, d = 0.0, 0.0, 0.0, 0.0
    if sumpt2 != 0:
        sumdeta /= sumpt2
        sumdphi /= sumpt2
        sumdeta2 /= sumpt2
        sumdphi2 /= sumpt2
        sumdetadphi /= sumpt2
        
        a = sumdeta2 - sumdeta*sumdeta
        b = sumdphi2 - sumdphi*sumdphi
        c = sumdeta*sumdphi - sumdetadphi
        ptd = ROOT.TMath.Sqrt(sumpt2)/sumpt
    d = np.sqrt(np.fabs((a-b)*(a-b)+4*c*c))
    axis1 = ROOT.TMath.Sqrt(0.5*(a+b+d)) if (a+b+d)>0 else 0.0
    axis2 = ROOT.TMath.Sqrt(0.5*(a+b-d)) if (a+b-d)>0 else 0.0
    axisA = ROOT.TMath.Sqrt(axis1*axis1 + axis2*axis2)
    # if momentGirth == 0: print("momentGirth is almost zero")
    momentGirth /= jet.PT
    # print(momentGirth)
    momentHalf /= jet.PT
    no_of_particles = len(jet.Constituents)
    
    return momentGirth, momentHalf, no_of_particles, ptd, axis1, axis2, axisA, sumpt2

def Normalizedhist(hist):
    if hist.GetSumw2N()==0: hist.Sumw2(True)
    if hist.Integral()!=0: 
        hist.Scale(1./hist.Integral())
    return hist


def GetMomentGirth(jet):
    momentgirth = 0
    for particle in jet.Particles:
        dphi = GetdPhi(particle.Phi, jet.Phi)
        deta = GetdEta(particle.Eta, jet.Eta)
        dR = GetdR(deta, dphi)
        pt = particle.PT
        momentgirth += pt*dR
    return momentgirth/jet.PT

def PrintStuffOut(jet):
    i = 0
    momentgirth, momentGirth = 0.0,0.0
    print("****************************************************************************************************************")
    print("===============       Jet Constituents        ==================== ")
    print("Jet info --- PT = {}, Eta = {}, Phi = {}, Mass = {}".format(jet.PT,jet.Eta,jet.Phi,jet.Mass))
    for particle in jet.Constituents:
        dphi = GetdPhi(particle.Phi, jet.Phi)
        deta = GetdEta(particle.Eta, jet.Eta)
        dR = GetdR(deta, dphi)
        momentgirth += particle.PT*dR
        print("type = {}, index = {} Constituents info --- PT = {}, Eta = {}, Phi = {}, Charge = {}, dphi = {}, deta {}, dR = {}".format(type(particle),i, particle.PT,particle.Eta,particle.Phi,particle.Charge,dphi,deta,dR))
        i+=1
    print("Moment girth calculated = {}".format(momentgirth/jet.PT))
    i = 0
    print("===============       Jet Particles        ==================== ")
    for particle in jet.Particles:
        dphi = GetdPhi(particle.Phi, jet.Phi)
        deta = GetdEta(particle.Eta, jet.Eta)
        dR = GetdR(deta, dphi)
        momentGirth += particle.PT*dR
        print("type = {}, index = {} Particles info --- PT = {}, Eta = {}, Phi = {}, Charge = {}, dphi = {}, deta {}, dR = {}".format(type(particle),i, particle.PT,particle.Eta,particle.Phi,particle.Charge,dphi,deta,dR))
        i+=1
    print("Moment girth calculated = {}".format(momentGirth/jet.PT))

def LoopOverJet(jet):

    momentgirth, momentGirth = 0.0,0.0
    print("****************************************************************************************************************")
    print("===============       Jet Constituents        ==================== ")
    print("Jet info --- PT = {}, Eta = {}, Phi = {}, Mass = {}".format(jet.PT,jet.Eta,jet.Phi,jet.Mass))
    for i in range(0, jet.Constituents.GetEntries()):
        particle = jet.Constituents.At(i)

        # if isinstance(particle,Track): print("it is a track")
        # if isinstance(particle,cppyy.gbl.Tower): print("it is a tower")
          
        # particle = object.Tower.GetObject()
        if particle == 0: continue
        dphi = GetdPhi(particle.Phi, jet.Phi)
        deta = GetdEta(particle.Eta, jet.Eta)
        dR = GetdR(deta, dphi)
        momentgirth += particle.PT*dR
        print("type = {}, index = {} Constituents info --- PT = {}, Eta = {}, Phi = {}, Charge = {}, dphi = {}, deta {}, dR = {}".format(type(particle),i, particle.PT,particle.Eta,particle.Phi,particle.Charge,dphi,deta,dR))

    print("Moment girth calculated = {}".format(momentgirth/jet.PT))

    for i in range(0, jet.Particles.GetEntries()):
        particle = jet.Constituents.At(i)
        if particle == 0: continue
        dphi = GetdPhi(particle.Phi, jet.Phi)
        deta = GetdEta(particle.Eta, jet.Eta)
        dR = GetdR(deta, dphi)
        momentGirth += particle.PT*dR
        print("type = {}, index = {} Particles info --- PT = {}, Eta = {}, Phi = {}, Charge = {}, dphi = {}, deta {}, dR = {}".format(type(particle),i, particle.PT,particle.Eta,particle.Phi,particle.Charge,dphi,deta,dR))

    print("Moment girth calculated = {}".format(momentGirth/jet.PT))

# TODO loop over the towers, -- does not work 
        
