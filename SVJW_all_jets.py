# Code for the analysis of the Semi-Visible Jets Workshop on 5-7 July 2022.
# Code focuses on gen level analysis of a Delphes file generated with hidden Valley pythia.

import sys
import numpy as np
import ROOT as root
from helpers import *

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 3:
  print(" Usage: Analysis_code/Jet_analysis.py /path/delphes_file.root /path/output.root R")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
        ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
        ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
        pass

inputFile = sys.argv[1]
print("Input file :")
print(inputFile)

outputFile = sys.argv[2]
print("output file :")
print(outputFile)

R = float(sys.argv[3])
print(" R used = {}".format(R))
print(type(R))

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

# Get pointer to branches used in this analysis
# R-jet branches : 04, 08, 10, 12, 14
R_jet = str(int(R*10))
if R<1.0: R_jet = '0' + R_jet
print("The R value used = {}".format(R_jet))

# Getting the required branches from Delphes ROOT file. 

# Reco level branches
branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R_jet)
branchMET = treeReader.UseBranch("MissingET")
branchHT = treeReader.UseBranch("ScalarHT")
branchTower = treeReader.UseBranch("Tower")
branchtrack = treeReader.UseBranch("Track")
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")

# Including gen level branches
branchGenMET = treeReader.UseBranch("GenMissingET")
branchGenJet = treeReader.UseBranch("GenJet%s"%R_jet)
branchParticle = treeReader.UseBranch("Particle")
branchElectron = treeReader.UseBranch("Electron")
branchPhoton = treeReader.UseBranch("Photon")
branchMuon = treeReader.UseBranch("Muon")

# book histograms
Nbins = 300
 