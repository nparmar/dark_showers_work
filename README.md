The code needs ```Delphes``` installed and linked such that delphes library can be invoked. 
Delphes can be found at
``` https://cp3.irmp.ucl.ac.be/projects/delphes```
In order for this to work, install Delphes and in your bashrc set following variables
```
export DELPHESPATH=<PATH_TO_DELPHES_INSTALLATION>
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DELPHESPATH
```
To run the analysis use the 

command: 
```
python /path_of_code/analysis.py /path_of_rootfile/name_of_rootfile.root /path_of_rootfile/output_name.root
```