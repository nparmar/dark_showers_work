R="R12"
echo "Working on $R"
r='1.2'

pi1="new_benchmarks/$R/Nc3Nf3_sFoff_1pi_decay_lam10_$R.root"
pi1_lam5="new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam5_$R.root"
pi1_lam50="new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam50_$R.root"

pi2="new_benchmarks/$R/Nc3Nf3_sFoff_2pi_decay_lam10_$R.root"
pi2_lam5="new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam5_$R.root"
pi2_lam50="new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam50_$R.root"

pi3="new_benchmarks/$R/Nc3Nf3_sFoff_3pi_decay_lam10_$R.root"
pi3_lam5="new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam5_$R.root"
pi3_lam50="new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam50_$R.root"

python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_1pi_decay_lam10_$R.root $r
python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam5_$R.root $r
python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam50_$R.root $r

python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_2pi_decay_lam10_$R.root $r
python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam5_$R.root $r
python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam50_$R.root $r

python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_3pi_decay_lam10_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam5_0_$R.root $r
python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam50_$R.root $r


# Plotting script
histlist=("njets" "jet1_mass_softdrop" "jet2_mass_softdrop" "momentgirth_j1" "momentgirth_j2" "momentgirth_j_all" "momenthalf_j1" "momenthalf_j2" "momenthalf_j_all" "multiplicity_j1" "multiplicity_j2" "multiplicity_j_all" "ptd_j1" "ptd_j2" "ptd_j_all" "majaxis_j1" "majaxis_j2" "majaxis_j_all" "minaxis_j1" "minaxis_j2" "minaxis_j_all" "avgaxis_j1" "avgaxis_j2" "avgaxis_j_all" "tau21_j1" "tau21_j2" "tau21_j_all" "tau32_j1" "tau32_j2" "tau32_j_all" "lha_j1" "lha_j2" "lha_j_all" "thrust_j1" "thrust_j2" "thrust_j_all" "ptd_j1_calc" "ptd_j2_calc" "ptd_j_all_calc")
pi_list=("1pi" "2pi")


# histlist=("ptd_j1_calc" "ptd_j2_calc" "ptd_j_all_calc")
# histlist=("tau21_j2" "tau21_j1")

save_plots_1pi="new_benchmarks/Plots/1pi/$R"
save_plots_2pi="new_benchmarks/Plots/2pi/$R"
save_plots_3pi="new_benchmarks/Plots/3pi/$R"
compare_lam_10="new_benchmarks/Plots/compare_lam_10/$R"
compare_lam_5="new_benchmarks/Plots/compare_lam_5/$R"
compare_lam_50="new_benchmarks/Plots/compare_lam_50/$R"

for hist in ${histlist[@]}
do
    python3.9 plotter_temp.py -l $pi1_lam5 $pi1 $pi1_lam50 -n $hist -s $save_plots_1pi/$hist -r $R
    python3.9 plotter_temp.py -l $pi2_lam5 $pi2 $pi2_lam50 -n $hist -s $save_plots_2pi/$hist -r $R
    python3.9 plot_overlay_2_files.py -l $pi3 $pi3_lam50 -n $hist -s $save_plots_3pi/$hist -r $R
    python3.9 plotter_temp.py -l $pi1 $pi2 $pi3 -n $hist -s $compare_lam_10/$hist -r $R
    python3.9 plot_overlay_2_files.py -l $pi1_lam5 $pi2_lam5 -n $hist -s $compare_lam_5/$hist -r $R
    python3.9 plotter_temp.py -l $pi1_lam50 $pi2_lam50 $pi3_lam50 -n $hist -s $compare_lam_50/$hist -r $R
done











