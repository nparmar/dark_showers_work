# Authors and contacts:
# Guillaume Albouy: guillaume.albouy@etu.univ-grenoble-alpes.fr
# Akanksha Singh: akki153209@gmail.com
# Harikrishnan Nair: hunair1996@gmail.com

# This code needs helpers.py and histo_defs.py which are stored in the same folder as this file
# Following cuts on events are assumed
# jet1.PT > 500 and np.abs(jet1.Eta) < 2.5 and jet2.PT > 500 and np.abs(jet2.Eta) < 2.5
# We assume that different branches corresponding to different jet radii are defined in root files
# In this code jet clustering radius is fixed to 1.4, can be changed at line 52
# This code will analyse input sample and create following reconstructed level normalized distributions:
# 1) pt of leading/subleading jet
# 2) dijet invarint mass
# 3) missing energy
# 4) transverse mass of dijet and met system
# 5) transverse ratio
# 6) delta phi between missin energy and leading/subleading jet
# 7) 2D histo of track pT of leading jet
# 8) delta eta between leading and subleading jet
# 9) pt and invariant mass for trimmed and SoftDropped leading/subleading jets
#
# command: python /path_of_code/transverse_mass.py /path_of_rootfile/name_of_rootfile.root /path_of_rootfile/output_name.root
# Takes the rootfile as input and computes the defined variables and fills the respective histograms.

# adding transverse dijet mass (mt_jj), r_T variable,



from statistics import NormalDist
import sys
from matplotlib.pyplot import contour
import numpy as np
import ROOT
from array import array
from helpers import *
import argparse

# parser = argparse.ArgumentParser()
# parser.add_argument('-r', '--R', dest='R', default=0.4,type=float,help='Radius of Jets')
# args = parser.parse_args()

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 3:
  print(" Usage: Analysis_code/Jet_analysis.py /path/delphes_file.root /path/output.root")
  sys.exit(1)



ROOT.gSystem.Load("libDelphes")

try:
        ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
        ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
        pass

# Parameters
############################################
# Radius of jets (0.4, 1.0, 1.4) :
# R = 1.4
# R = args.R
# Events selection (pT in GeV)
# pT_min_jet1 = 500
# pT_min_jet2 = 500
pT_min_jet1 = 10
pT_min_jet2 = 10
eta_max = 2.5
M_PI = 3.14
############################################

inputFile = sys.argv[1]
print("Input file :")
print(inputFile)

outputFile = sys.argv[2]
print("output file :")
print(outputFile)

R = float(sys.argv[3])
print(" R used = {}".format(R))
print(type(R))
# Create chain of root trees
chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

# Get pointer to branches used in this analysis
# R-jet branches : 04, 08, 10, 12, 14
R_jet = str(int(R*10))
if R<1.0: R_jet = '0' + R_jet
print("The R value used = {}".format(R_jet))
# Getting the required branches from Delphes ROOT file.
branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R_jet)
# branchJetsize = treeReader.UseBranch("ParticleFlowJet%s_size"%R_jet)
branchMET = treeReader.UseBranch("MissingET")
branchtrack = treeReader.UseBranch("Track")
branchHT = treeReader.UseBranch("ScalarHT")
branchGenJet = treeReader.UseBranch("GenJet%s"%R_jet)

# Book histograms

Nbins = 150
hist1Jet1PT = ROOT.TH1F("jet1_pt", ";p_{T} (j_{1}) GeV;Events", Nbins, 0.0, 800.0)
hist1Jet2PT = ROOT.TH1F("jet2_pt", ";p_{T} (j_{2}) GeV;Events", Nbins, 0.0, 800.0)
histMET = ROOT.TH1F("jet_met", "Missing transverse energy;MET GeV;Events", Nbins, .0, 2000.0)
hist1JetmJJ = ROOT.TH1F("jet_mJJ", " Invariant mass m_{JJ} with R=%.1f;m_{JJ} GeV;Events"%(R), Nbins, 0.0, 2500.0)

Nbins = 40
hist1Jet1Ntrk = ROOT.TH1F("jet1_ntrk", "Lead jet N_{trk} with R=%.1f;N_{trk};Events"%(R), Nbins, .0, 120.0)
hist1Jet2Ntrk = ROOT.TH1F("jet2_ntrk", "Sub jet N_{trk} with R=%.1f;N_{trk};Events"%(R), Nbins, .0, 120.0)

hist2D_ptd_ntrk_j1 = ROOT.TH2F("ptd_vs_ntrk","PTD vs Ntrack with with R=%.1f;p_{T}^{D};Ntracks"%(R),50,0.0,1.0,40,0,50)

Nbins = 12 
hnjets  = ROOT.TH1F("njets",";N_{jets};Events",Nbins, 0, 12)
# hnjets_after_cuts  = ROOT.TH1F("njets_after_cuts"," Number of Jets after cuts;N_{jets};Events",Nbins, 0, 10)


Nbins = 100
hdeltaR = ROOT.TH1F("deltaR","Delta R between jets and its constituents;#DeltaR;Events",Nbins, 0,4)
hgendeltaR = ROOT.TH1F("gendeltaR","Delta R between genjets and its constituents;#DeltaR;Events",Nbins, 0,4)

Nbins = 150
hmt = ROOT.TH1F("jet_met_mt" , "Transverse mass of jet1 + jet2 + MET;#frac{#cancel{E_{T}}}{m_{T}};Events" , Nbins, 0.0 ,5000.0)
hmt_jj = ROOT.TH1F("mt_jj",";m_{T}(j_{1}j_{2});Events",Nbins,0.0,3000.0)

Nbins = 50
hdphi1 = ROOT.TH1F("dPhi1", ";#Delta#phi_{j_{1},MET};Events", Nbins, 0.0, 3.4)
hdphi2 = ROOT.TH1F("dPhi2", ";#Delta#phi_{j_{2},MET};Events", Nbins, 0.0, 3.4)
hj1phi = ROOT.TH1F("j1phi", "Leading jet phi;#phi;Events",Nbins,-3.3,3.3)
hj2phi = ROOT.TH1F("j2phi", "Sub Leading jet phi;#phi;Events",Nbins,-3.3,3.3)

Nbins = 100
hrt = ROOT.TH1F("jet_met_rt" , ";R_{T} = #frac{MET}{M_{T}};Events" , Nbins, 0.0 , 0.6)
hht = ROOT.TH1F("HT", "Scalar HT;HT;Events", Nbins, 0.0,4000.0)


Nbins = 150
hist2Jet1PT = ROOT.TH1F("jet1_pt_softdrop", "Lead jet p_{T} with R=%.1f;p_{T}^{SD} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist2Jet2PT = ROOT.TH1F("jet2_pt_softdrop", "Sub jet p_{T} with R=%.1f;p_{T}^{SD} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist2JetmJJ = ROOT.TH1F("jet_mJJ_softdrop", " Invariant mass m_{JJ} with R=%.1f;m_{JJ}^{SD} GeV;Events"%(R), Nbins, 0.0, 2500.0)
hist2Jet1Mass = ROOT.TH1F("jet1_mass_softdrop", ";m^{SD}_{j_{1}} [GeV];Events", Nbins, 0.0, 500.0)
hist2Jet2Mass = ROOT.TH1F("jet2_mass_softdrop", ";m^{SD}_{j_{2}} [GeV];Events", Nbins, 0.0, 500.0)

hist3Jet1PT = ROOT.TH1F("jet1_pt_trimmed", "Lead jet p_{T} with R=%.1f;p_{T} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist3Jet2PT = ROOT.TH1F("jet2_pt_trimmed", "Sub jet p_{T} with R=%.1f;p_{T} GeV;Events"%(R), Nbins, 0.0, 2000.0)
hist3JetmJJ = ROOT.TH1F("jet_mJJ_trimmed", " Invariant mass m_{JJ} with R=%.1f;m_{JJ} GeV;Events"%(R), Nbins, 0.0, 3500.0)

histtrackpt = ROOT.TH2F("track_pt", "track PT vs Ntrk1;p_{T};Events", 20,0.0,100.0 , 40,0.0,160.0)

Nbins = 50
histdelEta = ROOT.TH1F("delta_eta", "delta_eta_jet1_and_jet2;#Delta#eta(j_{1},j_{2});Events", Nbins,0.0,4.0)
hj1eta = ROOT.TH1F("j1eta", "Leading jet eta;#eta(j_{1});Events",Nbins,0.0,4.0)
hj2eta = ROOT.TH1F("j2eta", "Sub Leading jet eta;#eta(j_{2});Events",Nbins,0.0,4.0)


Nbins = 100
hmomentgirth_1 = ROOT.TH1F("momentgirth_j1",";girth(j_{1});Events",Nbins,0.0, R)
hmomentgirth_2 = ROOT.TH1F("momentgirth_j2",";girth(j_{2});Events",Nbins,0.0, R)
hmomenthalf_1  = ROOT.TH1F("momenthalf_j1", ";Moment half (j_{1});Events",Nbins,0.0,R) 
hmomenthalf_2  = ROOT.TH1F("momenthalf_j2", ";Moment half (j_{2});Events",Nbins,0.0,R) 
hnpart_j1      = ROOT.TH1F("multiplicity_j1",";Multiplicity(j_{1});Events",Nbins,0,100)
hnpart_j2      = ROOT.TH1F("multiplicity_j2",";Multiplicity(j_{2});Events",Nbins,0,100)
hptd_j1        = ROOT.TH1F("ptd_j1",";p_{T}^{D}(j_{1});Events",Nbins,0,1.0)
hptd_j2        = ROOT.TH1F("ptd_j2",";p_{T}^{D}(j_{2});Events",Nbins,0,1.0)

hptd_j1_calc        = ROOT.TH1F("ptd_j1_calc","Calculated Leading Jet PtD;p_{T}^{D}(j_{1});Events",Nbins,0,1.0)
hptd_j2_calc        = ROOT.TH1F("ptd_j2_calc","Calculated Sub Leading Jet PtD;p_{T}^{D}(j_{2});Events",Nbins,0,1.0)

hgenptd_j1_calc        = ROOT.TH1F("gen_ptd_j1_calc","Calculated Leading Gen Jet PtD;p_{T}^{D}(j_{1});Events",Nbins,0,1.0)

hmajaxis_j1    = ROOT.TH1F("majaxis_j1",";#sigma_{major}(j_{1});Events",Nbins,0,R)
hmajaxis_j2    = ROOT.TH1F("majaxis_j2",";#sigma_{major}(j_{2});Events",Nbins,0,R)
hminaxis_j1    = ROOT.TH1F("minaxis_j1",";#sigma_{minor}(j_{1});Events",Nbins,0,R)
hminaxis_j2    = ROOT.TH1F("minaxis_j2",";#sigma_{minor}(j_{2});Events",Nbins,0,R)
havgaxis_j1    = ROOT.TH1F("avgaxis_j1",";#sigma_{avg}(j_{1});Events",Nbins,0,R)
havgaxis_j2    = ROOT.TH1F("avgaxis_j2",";#sigma_{avg}(j_{2});Events",Nbins,0,R)
htau1_j1       = ROOT.TH1F("tau1_j1","Leading Jet #tau_1;#tau_{1};Events",Nbins,0,1.0)
htau1_j2       = ROOT.TH1F("tau1_j2","Sub Leading Jet #tau_{1};#tau_{1};Events",Nbins,0,1.0)
htau2_j1       = ROOT.TH1F("tau2_j1","Leading Jet #tau_{2};#tau_{2};Events",Nbins,0,1.0)
htau2_j2       = ROOT.TH1F("tau2_j2","Sub Leading Jet #tau_{2};#tau_{2};Events",Nbins,0,1.0)
htau3_j1       = ROOT.TH1F("tau3_j1","Leading Jet #tau_{3};#tau_{3};Events",Nbins,0,1.0)
htau3_j2       = ROOT.TH1F("tau3_j2","Sub Leading Jet #tau_{3};#tau_{3};Events",Nbins,0,1.0)
htau21_j1      = ROOT.TH1F("tau21_j1",";#tau_{21}(j_{1});Events",Nbins,0,1.0)
htau21_j2      = ROOT.TH1F("tau21_j2",";#tau_{21}(j_{2});Events",Nbins,0,1.0)
htau32_j1      = ROOT.TH1F("tau32_j1",";#tau_{32}(j_{1});Events",Nbins,0,1.0)
htau32_j2      = ROOT.TH1F("tau32_j2",";#tau_{32}(j_{2});Events",Nbins,0,1.0)

hgenmajaxis_j1 = ROOT.TH1F("gen_majaxis_j1",";#sigma_{major}(gen j_{1});Events",Nbins,0,3.0)
hgenminaxis_j1 = ROOT.TH1F("gen_minaxis_j1",";#sigma_{minor}(gen j_{1});Events",Nbins,0,3.0)
hgenmomentgirth_1 = ROOT.TH1F("gen_momentgirth_j1",";girth(gen j_{1});Events",Nbins,0.0, 3.0)




# hfrac_pt       = ROOT.TH1F("fracpt","Frac pt;")
# TODO add more variables like Energy fraction stuff. 

counter_j1 = 0 
counter_j2 = 0

# njetsbranch = treeReader.UseBranch("ParticleFlowJet%s_size"%R_jet)
# Loop over all events
for entry in range(0, numberOfEntries):
    # Load selected branches with data from specified event
    treeReader.ReadEntry(entry)
    # njets = treeReader.UseBranch("ParticleFlowJet%s@.size()"%R_jet)
    njets = branchJet.GetEntries()
    # njets = chain.GetLeaf("ParticleFlowJet%s_size"%R_jet)
    # print(njets)   
    # If event contains at least 2 jets

    for i in range(0,branchJet.GetEntries()):
      for constituents in branchJet.At(i).Constituents:
        tempjet = branchJet.At(i)
        deta = GetdEta(constituents.Eta,tempjet.Eta)
        dphi = GetdPhi(constituents.Phi,tempjet.Phi)
        dR = GetdR(deta,dphi)
        hdeltaR.Fill(dR)
        # print("type = {}, index = {} Constituents info --- PT = {}, Eta = {}, Phi = {}, Charge = {}, dphi = {}, deta {}, dR = {}".format(type(constituents),i, constituents.PT,constituents.Eta,constituents.Phi,constituents.Charge,dphi,deta,dR))
        

    for i in range(0,branchGenJet.GetEntries()):
      for constituent in branchGenJet.At(i).Constituents:
        tempgjet = branchGenJet.At(i)
        gdeta = GetdEta(constituent.Eta,tempgjet.Eta)
        gdphi = GetdPhi(constituent.Phi,tempgjet.Phi)
        gdR = GetdR(gdeta,gdphi)
        hgendeltaR.Fill(gdR)


    if branchJet.GetEntries() > 1:
        # Take the two leading jets
        jet1 = branchJet.At(0)
        jet2 = branchJet.At(1)
        # print("Number of jets = {}".format(branchJet.GetEntries()))
        if jet1.PTD == 1 or jet2.PTD == 1:
          continue
          
        #Selecting events
        if jet1.PT > pT_min_jet1 and np.abs(jet1.Eta) < eta_max and jet2.PT > pT_min_jet2 and np.abs(jet2.Eta) < eta_max :
            # njets_after_cuts = branchJet.GetEntries()
            # Defining the TLorentz vector for leading jet
            vec1 = GetJetVector(jet1.PT , jet1.Eta , jet1.Phi , jet1.Mass)

            # Defining TLorentz vector for sub - leading jet
            vec2 = GetJetVector(jet2.PT , jet2.Eta , jet2.Phi , jet2.Mass)

            # Defining TLorentz vector for missing energy branch
            #{
            met = branchMET.At(0)
            m = met.MET
            METPhi = met.Phi
            METx = m* np.cos(METPhi)
            METy = m* np.sin(METPhi)
            vecmet = ROOT.TLorentzVector()
            vecmet.SetPxPyPzE(METx , METy , 0 , m)
            #}
            htleaf = branchHT.At(0)
            ht = htleaf.HT


            # Computing transverse mass for jet1+jet2+met
            mt = (vec1 + vec2 + vecmet).Mt()

            # Computing transverse mass for jet1 and jet2
            mt_jj = (vec1 + vec2).Mt()

            # Computing dPhi between jet1 and met
            dPhi1 = GetdPhi(jet1.Phi, met.Phi)

            # Computing dphi between jet2 and met
            dPhi2 = GetdPhi(jet2.Phi , met.Phi)

            # Computing transverse ratio (R_T) # TODO change it to R!
            if (mt!=0): rt = m/mt

            # SoftDropped jets
            #{
            # Getting the Soft dropped jet1 four momenta
            jet1_softdrop = jet1.SoftDroppedP4[0]
            jet1_pt_softdrop = jet1_softdrop.Pt()
            jet1_mass_sofdrop = jet1_softdrop.M()
            # Getting the Soft dropped Jet2 four momenta
            jet2_softdrop = jet2.SoftDroppedP4[0]
            jet2_pt_softdrop = jet2_softdrop.Pt()
            jet2_mass_softdrop = jet2_softdrop.M()
            # Computing invariant mass using Soft dropped leading and sub-leading jets
            invmass_softdrop = Getinvmass(jet1_softdrop , jet2_softdrop)
            #}
            
            # N-subjettiness 

            # print("Tau 1 = {}",jet1.Tau[0])
            # print(type(jet1.Tau))
            # print("Taus 2 = ",jet1.Tau[1])
            # print("Tau 3 = {}",jet1.Tau[2])
            # print("Tau 2,1 = {}",jet1.Tau[1]/jet1.Tau[0])  
            # print("Tau 3,2 = {}",jet1.Tau[2]/jet1.Tau[1])

            tau1_j1  = jet1.Tau[0]
            tau1_j2  = jet2.Tau[0]
            tau2_j1  = jet1.Tau[1]
            tau2_j2  = jet2.Tau[1]
            tau3_j1  = jet1.Tau[2]
            tau3_j2  = jet2.Tau[2]
            tau21_j1 = tau2_j1/tau1_j1 if tau1_j1 > 0.0 else 0.0
            tau21_j2 = tau2_j2/tau1_j2 if tau1_j2 > 0.0 else 0.0
            tau32_j1 = tau3_j1/tau2_j1 if tau2_j1 > 0.0 else 0.0
            tau32_j2 = tau3_j2/tau2_j2 if tau2_j2 > 0.0 else 0.0

            # print(" ####################################################################################################################")
            # # PrintStuffOut(jet1)
            # LoopOverJet(jet1)


    
            # # jets = [jet1, jet2]
            # 
            # momentgirth_1 = 0.0
            # momenthalf_1, sumpt, sumpt2, sumdeta, sumdphi, sumdeta2, sumdphi2, sumdetadphi = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            # no_of_part_1 = 0
            # ptd_1_calc, axis1_1, axis2_2, axisA_1 = 0.0,0.0,0.0,0.0
            # # print("jet values --  pt = {}, eta = {}, phi = {}".format(jet.PT,jet.Eta,jet.Phi))
            # for particle in jet.Constituents:
            #   dphi = GetdPhi(particle.Phi, jet.Phi)
            #   deta = GetdEta(particle.Eta, jet.Eta)
            #   dR = GetdR(deta, dphi)
            #   # dphi = particle.Phi
            #   # deta = particle.Eta
            #   # dR   = GetdR(dphi,deta)
            #   # print("particle values --  pt = {}, eta = {}, phi = {}".format(particle.PT,particle.Eta,particle.Phi))
            #   # print("dR = {}, deta = {}, dphi = {}".format(dR,deta,dphi))
            #   pT = particle.PT
            #   if pT == 0: print("particle pt is zero")
            #   pt2 = pT**2
            #   if pt2 == 0: print("particle pt2 is zero")
            #   # momentgirth_1 += pT*dR 
            #   # if momentgirth_1 == 0: print("momentGirth is zero")
            #   momenthalf_1 += pT*ROOT.TMath.Sqrt(dR)
            #   sumpt += pT
            #   sumpt2 += pt2
            #   sumdeta += deta*pt2
            #   sumdphi += dphi*pt2
            #   sumdeta2 += deta*deta*pt2
            #   sumdphi2 += dphi*dphi*pt2
            #   sumdetadphi += deta*dphi*pt2
            #   if sumpt2 == 0:
            #     print("particle values --  pt = {}, eta = {}, phi = {}".format(particle.PT,particle.Eta,particle.Phi))
            #     print("dR = {}, deta = {}, dphi = {}".format(dR,deta,dphi))  
            # if sumpt2 != 0:
            #   # print("particle values --  pt = {}, eta = {}, phi = {}".format(particle.PT,particle.Eta,particle.Phi))
            #   # print("dR = {}, deta = {}, dphi = {}".format(dR,deta,dphi))
              
            # #   sumdeta /= sumpt2
            # #   sumdphi /= sumpt2
            # #   sumdeta2 /= sumpt2
            # #   sumdphi2 /= sumpt2
            # #   sumdetadphi /= sumpt2
            #   a, b, c, d = 0.0, 0.0, 0.0, 0.0
            #   a = sumdeta2 + sumdphi2
            #   b = sumdeta2*sumdphi2
            #   c = sumdetadphi**2
            #   d = ROOT.TMath.Sqrt(a**2 - 4*(b-c))
              
            #   axis1_1 = ROOT.TMath.Sqrt(ROOT.TMath.Sqrt(0.5*(a+d))/sumpt2) if (a+d)>0 else 0.0
            #   axis2_1 = ROOT.TMath.Sqrt(ROOT.TMath.Sqrt(0.5*(a+d))/sumpt2) if (a-d)>0 else 0.0
            #   axisA_1 = ROOT.TMath.Sqrt(axis1_1*axis1_1 + axis2_1*axis2_1)
            #   # if momentGirth == 0: print("momentGirth is almost zero")
            #   # momentgirth_1 /= jet.PT
            #   # print(momentGirth)
            #   momenthalf_1 /= jet.PT
            #   no_of_part_1 = len(jet.Constituents)
            #   ptd_1_calc = ROOT.TMath.Sqrt(sumpt2)/sumpt
            

            # momentgirth_1 = GetMomentGirth(jet1)

            # print(jet1.Constituents.At(1).PT)
            momentgirth_1, momenthalf_1, no_of_part_1, ptd_1_calc, axis1_1, axis2_1, axisA_1, sumpt2j1 = GetJetvariables(jet1,R)
            momentgirth_2, momenthalf_2, no_of_part_2, ptd_2_calc, axis1_2, axis2_2, axisA_2, sumpt2j2 = GetJetvariables(jet2,R)  


            gen_momentgirth_1, gen_momenthalf_1, gen_no_of_part_1, gen_ptd_1_calc, gen_axis1_1, gen_axis2_1, gen_axisA_1, gen_sumpt2j1 = GetJetvariables(jet1,R)
            


            if sumpt2j1 == 0: 
              counter_j1+=1
              continue
            if sumpt2j2 == 0:
              counter_j2+=1
              continue
            # Jet PTD 
            ptd_1 = jet1.PTD
            ptd_2 = jet2.PTD

            # print(momentgirth_1)

            # Trimmed jets
            #{
            # Getting the Trimmed jet1 four momenta
            jet1_trimmed = jet1.TrimmedP4[0]
            jet1_pt_trimmed = jet1_trimmed.Pt()

            # Getting the Trimmed jet2 four momenta
            jet2_trimmed = jet2.TrimmedP4[0]
            jet2_pt_trimmed = jet2_trimmed.Pt()

            # Computing invariant mass using Trimmed leading and sub-leading jets
            invmass_trimmed = Getinvmass(jet1_trimmed , jet2_trimmed)
            #}

            #Calculating average track transverse momentum
            #{
            delEta = GetdEta(jet1.Eta , jet2.Eta)
            track = []
            selectedtrack = []
            trackpt = []
            #Store the tracks to array called "track"
            for i in range(0 , branchtrack.GetEntries()):
                 track.append(branchtrack.At(i))

            #Loop over array "track" and calculate delta phi and delta R
            for j in range(0 , len(track)):
                dPhi = GetdPhi(track[j].Phi , jet1.Phi)

                dEta = GetdEta(track[j].Eta , jet1.Eta)

                DeltaR = np.sqrt(dEta*dEta + dPhi*dPhi)

                #Store the tracks which satisfy the condition on delta R to array "selectedtrack"
                if DeltaR < 1.4 :
                    selectedtrack.append(track[j])

            #Loop over selected tracks to calculate average momentum of the tracks
            for k in range(0 , len(selectedtrack)):
                trackpt.append(selectedtrack[k].PT)

            averagept = np.average(trackpt)
            #}

            #Fill histograms
            hist1Jet1PT.Fill(jet1.PT)
            hist1Jet2PT.Fill(jet2.PT)
            hist1JetmJJ.Fill((jet1.P4() + jet2.P4()).M())
            hist1Jet1Ntrk.Fill(jet1.NCharged)
            hist1Jet2Ntrk.Fill(jet2.NCharged)
            histMET.Fill(branchMET.At(0).MET)
            hmt.Fill(mt)
            hmt_jj.Fill(mt_jj)
            hdphi1.Fill(dPhi1)
            hdphi2.Fill(dPhi2)
            hrt.Fill(rt)
            hist2Jet1PT.Fill(jet1_pt_softdrop)
            hist2Jet2PT.Fill(jet2_pt_softdrop)
            hist2JetmJJ.Fill(invmass_softdrop)
            hist2Jet1Mass.Fill(jet1_mass_sofdrop)
            hist2Jet2Mass.Fill(jet2_mass_softdrop)
            hist3Jet1PT.Fill(jet1_pt_trimmed)
            hist3Jet2PT.Fill(jet2_pt_trimmed)
            hist3JetmJJ.Fill(invmass_trimmed)
            histdelEta.Fill(delEta)
            histtrackpt.Fill(averagept,jet1.NCharged)
            hj1eta.Fill(jet1.Eta)
            hj1phi.Fill(jet1.Phi)
            hj2eta.Fill(jet2.Eta)
            hj2phi.Fill(jet2.Phi)
            hht.Fill(ht)
            hmomentgirth_1.Fill(momentgirth_1)
            hmomentgirth_2.Fill(momentgirth_2)
            hmomenthalf_1.Fill(momenthalf_1)
            hmomenthalf_2.Fill(momenthalf_2)
            hnpart_j1.Fill(no_of_part_1)
            hnpart_j2.Fill(no_of_part_2)
            hptd_j1.Fill(ptd_1)
            hptd_j2.Fill(ptd_2)

            hptd_j1_calc.Fill(ptd_1_calc)
            hptd_j2_calc.Fill(ptd_2_calc)

            hmajaxis_j1.Fill(axis1_1)
            hmajaxis_j2.Fill(axis1_2)
            hminaxis_j1.Fill(axis2_1)
            hminaxis_j2.Fill(axis2_2)
            havgaxis_j1.Fill(axisA_1)
            havgaxis_j2.Fill(axisA_2)
            htau1_j1.Fill(tau1_j1)
            htau1_j2.Fill(tau1_j2)
            htau2_j1.Fill(tau2_j1)
            htau2_j2.Fill(tau2_j2)
            htau3_j1.Fill(tau3_j1)
            htau3_j2.Fill(tau3_j2)
            htau21_j1.Fill(tau21_j1)
            htau21_j2.Fill(tau21_j2)
            htau32_j1.Fill(tau32_j1)
            htau32_j2.Fill(tau32_j2)
            hist2D_ptd_ntrk_j1.Fill(ptd_1,jet1.NCharged)
            hgenmajaxis_j1.Fill(gen_axis1_1)
            hgenminaxis_j1.Fill(gen_axis2_1)
            hgenmomentgirth_1.Fill(gen_momentgirth_1)
            hgenptd_j1_calc.Fill(gen_ptd_1_calc)
            # hnjets_after_cuts.Fill(njets_after_cuts)
    hnjets.Fill(njets)

            
#Printing number of accepted events
integral=hmt.Integral(0,-1)
print("integral=",integral)
print("jet 1 with the problem {}".format(counter_j1))
print("jet 2 with the problem {}".format(counter_j2))
print(numberOfEntries)

#Normalizing the histogram
if hist1Jet1PT.GetSumw2N()==0: hist1Jet1PT.Sumw2(True)
if hist1Jet2PT.GetSumw2N()==0: hist1Jet2PT.Sumw2(True)
if hist1JetmJJ.GetSumw2N()==0: hist1JetmJJ.Sumw2(True)
if hist1Jet1Ntrk.GetSumw2N()==0: hist1Jet1Ntrk.Sumw2(True)
if hist1Jet2Ntrk.GetSumw2N()==0: hist1Jet2Ntrk.Sumw2(True)
if histMET.GetSumw2N()==0: histMET.Sumw2(True)
if hmt.GetSumw2N()==0: hmt.Sumw2(True)
if hmt_jj.GetSumw2N()==0: hmt_jj.Sumw2(True)
if hdphi1.GetSumw2N()==0: hdphi1.Sumw2(True)
if hdphi2.GetSumw2N()==0: hdphi2.Sumw2(True)
if hrt.GetSumw2N()==0: hrt.Sumw2(True)
if hist2Jet1PT.GetSumw2N()==0: hist2Jet1PT.Sumw2(True)
if hist2Jet2PT.GetSumw2N()==0: hist2Jet2PT.Sumw2(True)
if hist2JetmJJ.GetSumw2N()==0: hist2JetmJJ.Sumw2(True)
if hist3Jet1PT.GetSumw2N()==0: hist3Jet1PT.Sumw2(True)
if hist3Jet2PT.GetSumw2N()==0: hist3Jet2PT.Sumw2(True)
if hist3JetmJJ.GetSumw2N()==0: hist3JetmJJ.Sumw2(True)
if histdelEta.GetSumw2N()==0: histdelEta.Sumw2(True)

if hj1eta.GetSumw2N()==0: hj1eta.Sumw2(True)
if hj2eta.GetSumw2N()==0: hj2eta.Sumw2(True)
if hj1phi.GetSumw2N()==0: hj1phi.Sumw2(True)
if hj2phi.GetSumw2N()==0: hj2phi.Sumw2(True)
if hht.GetSumw2N()==0: hht.Sumw2(True)
if hmomentgirth_1.GetSumw2N()==0: hmomentgirth_1.Sumw2(True)
if hmomentgirth_2.GetSumw2N()==0: hmomentgirth_2.Sumw2(True)
if hmomenthalf_1.GetSumw2N()==0: hmomenthalf_1.Sumw2(True)
if hmomenthalf_2.GetSumw2N()==0: hmomenthalf_2.Sumw2(True)


hist1Jet1PT.Scale(1./hist1Jet1PT.Integral())
hist1Jet2PT.Scale(1./hist1Jet2PT.Integral())
hist1JetmJJ.Scale(1./hist1JetmJJ.Integral())
hist1Jet1Ntrk.Scale(1./hist1Jet1Ntrk.Integral())
hist1Jet2Ntrk.Scale(1./hist1Jet2Ntrk.Integral())
histMET.Scale(1./histMET.Integral())
hmt.Scale(1./hmt.Integral())
hmt_jj.Scale(1./hmt_jj.Integral())
hdphi1.Scale(1./hdphi1.Integral())
hdphi2.Scale(1./hdphi2.Integral())
hrt.Scale(1./hrt.Integral())
hist2Jet1PT.Scale(1./hist2Jet1PT.Integral())
hist2Jet2PT.Scale(1./hist2Jet2PT.Integral())
hist2JetmJJ.Scale(1./hist2JetmJJ.Integral())
hist3Jet1PT.Scale(1./hist3Jet1PT.Integral())
hist3Jet2PT.Scale(1./hist3Jet2PT.Integral())
hist3JetmJJ.Scale(1./hist3JetmJJ.Integral())
histdelEta.Scale(1./histdelEta.Integral())

hj1eta.Scale(1./hj1eta.Integral())
hj2eta.Scale(1./hj2eta.Integral())
hj1phi.Scale(1./hj1phi.Integral())
hj2phi.Scale(1./hj2phi.Integral())
hht.Scale(1./hht.Integral())



hnpart_j1 = Normalizedhist(hnpart_j1)
hnpart_j2 = Normalizedhist(hnpart_j2)
hptd_j1   = Normalizedhist(hptd_j1)
hptd_j2   = Normalizedhist(hptd_j2)

hptd_j1_calc   = Normalizedhist(hptd_j1_calc)
hptd_j2_calc   = Normalizedhist(hptd_j2_calc)

hmajaxis_j1=Normalizedhist(hmajaxis_j1)
hmajaxis_j2=Normalizedhist(hmajaxis_j2)
hminaxis_j1=Normalizedhist(hminaxis_j1)
hminaxis_j2=Normalizedhist(hminaxis_j2)
havgaxis_j1=Normalizedhist(havgaxis_j1)
havgaxis_j2=Normalizedhist(havgaxis_j2)
htau1_j1  = Normalizedhist(htau1_j1)
htau2_j1  = Normalizedhist(htau2_j1)
htau3_j1  = Normalizedhist(htau3_j1)
htau21_j1 = Normalizedhist(htau21_j1)
htau32_j1 = Normalizedhist(htau32_j1)
htau1_j2  = Normalizedhist(htau1_j2)
htau2_j2  = Normalizedhist(htau2_j2)
htau3_j2  = Normalizedhist(htau3_j2)
htau21_j2 = Normalizedhist(htau21_j2)
htau32_j2 = Normalizedhist(htau32_j2)
hist2Jet1Mass = Normalizedhist(hist2Jet1Mass)
hist2Jet2Mass = Normalizedhist(hist2Jet2Mass)
hnjets = Normalizedhist(hnjets)
hdeltaR = Normalizedhist(hdeltaR)
hgendeltaR = Normalizedhist(hgendeltaR)
hgenmajaxis_j1 = Normalizedhist(hgenmajaxis_j1)
hgenminaxis_j1 = Normalizedhist(hgenminaxis_j1)
hgenmomentgirth_1 = Normalizedhist(hgenmomentgirth_1)
hgenptd_j1_calc = Normalizedhist(hgenptd_j1_calc)
# hnjets_after_cuts = Normalizedhist(hnjets_after_cuts)

#Creating a list and saving the histograms to the list.

histlist = ROOT.TList()

histlist.Add(hist1Jet1PT)
histlist.Add(hist1Jet2PT)
histlist.Add(hist1JetmJJ)
histlist.Add(hist1Jet1Ntrk)
histlist.Add(hist1Jet2Ntrk)
histlist.Add(histMET)
histlist.Add(hmt)
histlist.Add(hmt_jj)
histlist.Add(hdphi1)
histlist.Add(hdphi2)
histlist.Add(hrt)
histlist.Add(hist2Jet1PT)
histlist.Add(hist2Jet2PT)
histlist.Add(hist2JetmJJ)
histlist.Add(hist3Jet1PT)
histlist.Add(hist3Jet2PT)
histlist.Add(hist3JetmJJ)
histlist.Add(histtrackpt)
histlist.Add(histdelEta)

histlist.Add(hj1eta)
histlist.Add(hj1phi)
histlist.Add(hj2eta)
histlist.Add(hj1phi)
histlist.Add(hht)
histlist.Add(hmomentgirth_1)
histlist.Add(hmomentgirth_2)
histlist.Add(hmomenthalf_1)
histlist.Add(hmomenthalf_2)
histlist.Add(hnpart_j1)
histlist.Add(hnpart_j2)
histlist.Add(hptd_j1)
histlist.Add(hptd_j2)

histlist.Add(hptd_j1_calc)
histlist.Add(hptd_j2_calc)

histlist.Add(hmajaxis_j1)
histlist.Add(hmajaxis_j2)
histlist.Add(hminaxis_j1)
histlist.Add(hminaxis_j2)
histlist.Add(havgaxis_j1)
histlist.Add(havgaxis_j2)

histlist.Add(htau1_j1)
histlist.Add(htau2_j1)
histlist.Add(htau3_j1)
histlist.Add(htau21_j1)
histlist.Add(htau32_j1)
histlist.Add(htau1_j2)
histlist.Add(htau2_j2)
histlist.Add(htau3_j2)
histlist.Add(htau21_j2)
histlist.Add(htau32_j2)
histlist.Add(hist2Jet1Mass)
histlist.Add(hist2Jet2Mass)
histlist.Add(hnjets)
histlist.Add(hist2D_ptd_ntrk_j1)
histlist.Add(hdeltaR)
histlist.Add(hgendeltaR)
histlist.Add(hgenmajaxis_j1)
histlist.Add(hgenminaxis_j1)
histlist.Add(hgenmomentgirth_1)
histlist.Add(hgenptd_j1_calc)
# histlist.Add(hnjets_after_cuts)

#outputFile = inputFile[:-5] + "_combined_R14.root"
rootFile = ROOT.TFile(outputFile, "RECREATE")
histlist.Write()
rootFile.Close()
