#!/usr/bin/env python

import sys

import ROOT
from helpers import *

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 2:
  print(" Usage: Example1.py input_file")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
  ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
  ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
  pass

inputFile = sys.argv[1]
print("Input file = ",inputFile)
# Create chain of root trees

chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

# Get pointers to branches used in this analysis
branchJet = treeReader.UseBranch("ParticleFlowJet04")

# Loop over all events
for entry in range(0, 20):
  # Load selected branches with data from specified event
  treeReader.ReadEntry(entry)
  print("the event number is  =  ",entry)

  for i in range(0,branchJet.GetEntries()):
      for constituents in branchJet.At(i).Constituents:
        tempjet = branchJet.At(i)
        deta = GetdEta(constituents.Eta,tempjet.Eta)
        dphi = GetdPhi(constituents.Phi,tempjet.Phi)
        dR = GetdR(deta,dphi)
        print("something")
        print("Delta R = ",dR)


 
