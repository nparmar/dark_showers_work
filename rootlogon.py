import ROOT

print("*** Using default ROOTLOGON file ~/.rootlogon.C \n");
  TStyle *Mine  = new TStyle("Mine","My modification of Plain Style");
  // Take Plain defaults to avoid colored backgrounds...                                                                                                                                                    

  Mine->SetCanvasBorderMode(0);
  Mine->SetPadBorderMode(0);
  Mine->SetPadColor(0);
  Mine->SetFrameFillColor(0);
  Mine->SetCanvasColor(0);
  Mine->SetTitleColor(0);
  Mine->SetStatColor(0);
  /// And set it:                                                                                                                                                                                           
  gROOT->SetStyle("Mine");
  /// Now customize it:                                                                                                                                                                                     
  /// See: http://root.cern.ch/root/html/TStyle.html                                                                                                                                                        
  ///      to check all possibilities                                                                                                                                                                       
  Mine->SetHistLineWidth(2);     // Make default histogram line thicker                                                                                                                                     
  Mine->SetLineStyleString(20,"60 20"); // Create a new type of dashed line: ID=20                                                                                                                          
  Mine->SetTitleBorderSize(1);
  Mine->SetPalette(1); // Makes rainbow-style colors instead of default                                                                                                                                     
  // Size of axis labels and names:                                                                                                                                                                         
  Mine->SetLabelSize(0.065,"XYZ"); // default is 0.04                                                                                                                                                       
  Mine->SetTitleSize(0.065,"XYZ"); // default is 0.04                                                                                                                                                       
  // Since we are going to put the statbox inside the frame, we can use more space in                                                                                                                       
  // the pad. This moves the pad a little to the right:                                                                                                                                                     
  Mine->SetPadLeftMargin( .135); // default is 10%                                                                                                                                                          
  Mine->SetPadRightMargin(.075); // default is 10%                                                                                                                                                          
  // Now we need to make the XTitle size bigger so we move the pad up:                                                                                                                                      
  Mine->SetPadBottomMargin(.150); // 0.140                                                                                                                                                                  
  Mine->SetPadTopMargin(.085); // 0.85                                                                                                                                                                      
  Mine->SetCanvasBorderSize(0);
  Mine->SetFrameBorderMode(0); // Frame border to avoid annoying red/yellow lines                                                                                                                           
  // Draw stat box like PAW (no variable name and no offset for box)                                                                                                                                        
  Mine->SetStatX(0.925);  // top right corner (given the pad recentering above)                                                                                                                             
  Mine->SetStatY(0.915);  // upper line (given the pad enlargement above)                                                                                                                                   
  Mine->SetStatW(0.2);    // width of stat box (default 0.19)                                                                                                                                               
  Mine->SetStatBorderSize(1); // (default 2) The statbox is a TPave (with shadow),                                                                                                                          
  //Mine->SetStatStyle(0);      //  this reverts it to TBox                                                                                                                                                 
  Mine->SetOptStat(1110); // integral overflow underflow RMS mean entries name                                                                                                                              
  Mine->SetOptFit(111);   // prob chi2/ndof errors parameters-names                                                                                                                                         
  //Mine->SetStatStyle(4050); // makes legend transparent if 4000<x<4100                                                                                                                                    
  // Set the color and font type of title/axis:                                                                                                                                                             
  //Mine->SetTitleColor(0);       // Bkg color of title box                                                                                                                                                 
  Mine->SetTitleColor(1,"XYZ"); // Axis names color (only for version >= 3.05/04)                                                                                                                           
  //Mine->SetTitleTextColor(1);   // Font color of title                                                                                                                                                    
  // Set the title geometry:                                                                                                                                                                                
  Mine->SetTitleX(.135);
  Mine->SetTitleY(0.965);
  Mine->SetTitleW(.79); // Covers 79% of the X of the pad                                                                                                                                                   
  Mine->SetTitleBorderSize(0);
  Mine->SetTitleStyle(-1); // Make transpatent title                                                                                                                                                        
  //Mine->SetErrorX(0); // no horizontal error bar                                                                                                                                                          
  // How can I change the settings for legends globally?                                                                                                                                                    
// legend->SetBorderSize(0); legend->SetTextSize(0.07);                                                                                                                                                   
  // legend->SetTextFont(62);  legend->SetFillColor(0);                                                                                                                                                     
  // And what about the default marker?                                                                                                                                                                     
  // h1->SetMarkerStyle(20);                                                                                                                                                                                
  // h1->SetMarkerSize(0.4);                                                                                                                                                                                
  Mine->SetLegendBorderSize(0);
  //Mine->SetLegendFillColor(0);                                                                                                                                                                            
  //Mine->SetLegendFont(62);                                                                                                                                                                                
  Mine->SetHistFillColor(0);
  Mine->SetHistLineWidth(2);
  //                                                                                                                                                                                                        
  gROOT->ForceStyle();
