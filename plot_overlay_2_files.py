## Just a temporary file for overlaying the plots with hard coding everything

from ast import Add
from matplotlib.pyplot import hist
import numpy as np
import ROOT as root
import argparse

def AddOverflowBin(hist):
    entries = hist.GetEntries() # making sure the entries are counted
    numbins = hist.GetNbinsX() # Get the number of bins get the bin contents of the overflow bin
    hicontent = hist.GetBinContent(numbins) # Information necessary so that the last overflow bin contents can be added to the last bin
    overflow = hist.GetBinContent(numbins+1) # Getting the overflow bin content
    locontent = hist.GetBinContent(1) # Not useful here but if you need to also get the underflow printed
    underflow = hist.GetBinContent(0) # Getting the underflow bin content 
    if overflow > 0:
        hist.SetBinContent(numbins,hicontent+overflow) # 
        hist.SetEntries(entries) # restore the number of entries.
    
    # if underflow > 0:
    #     hist.SetBinContent(1,locontent+underflow)
    #     hist.SetEntries(entries)
    return hist

parser = argparse.ArgumentParser()
parser.add_argument('-l','--list', nargs='+', dest='rootfiles',help='root files', default=None, type=str )
parser.add_argument('-n','--histname', dest='histname',help='histogram',default=None,type=str)
parser.add_argument('-s','--save',dest='savename',help='save fig name',default=None,type=str)
parser.add_argument('-r','--radius', dest='R',help='radius of the Jet', default=None,type=str)

args = parser.parse_args()
samplelist = args.rootfiles
histname = args.histname
savename = args.savename
R = args.R

rootfile1 = root.TFile(samplelist[0])
rootfile2 = root.TFile(samplelist[1])

print("Input files are - {} \n {}".format(samplelist[0],samplelist[1]))

hist1 = rootfile1.Get(histname)
hist2 = rootfile2.Get(histname)

canvas = root.TCanvas("canvas","Canvas",700,500)
root.gROOT.LoadMacro("rootlogon.C")
hist1 = AddOverflowBin(hist1)
hist2 = AddOverflowBin(hist2)

# Plot the histograms

# hist1.SetTitle(hist1.GetTitle())
# hist1.GetXaxis.SetTitle(hist1.GetXaxis.GetTitle())
# hist1.GetYaxis.SetTitle(hist1.GetYaxis.GetTitle())
hist1.SetTitle("")
hist1.GetXaxis().SetLabelSize(0.065)
hist1.GetYaxis().SetLabelSize(0.065)
hist1.GetXaxis().SetTitleSize(0.065)
hist1.GetYaxis().SetTitleSize(0.065)

root.gPad.SetLeftMargin(0.165)
root.gPad.SetBottomMargin(0.15)
root.gPad.SetRightMargin(0.065)
root.gPad.SetTopMargin(0.085)

# Add overflow
binmax = hist1.GetMaximumBin()
ybinmax = hist1.GetBinContent(binmax)
hist1.SetMaximum(ybinmax + 8*ybinmax)

# hist1.GetYaxis().SetRange(0,binmax + 0.1*binmax)


hist1.SetStats(0)
hist1.SetLineColor(8)
hist1.SetLineWidth(2)
hist1.SetLineStyle(1)
hist1.Draw("hist")

hist2.SetLineColor(9)
hist2.SetLineWidth(2)
hist2.SetStats(0)
hist2.SetLineStyle(5)
hist2.Draw("hist sames")
root.gPad.SetLogy()


# Legends

# legend_names = [x.replace('snowmass_delphes/output/','').replace('.root','').replace('_0_',' ').replace('_',' ').replace('pi','#pi').replace('lam','#Lambda= ').replace('Nc3Nf3','').replace('sFoff','').replace('decay','').replace('probvec','probvec=').replace('05','0.5').replace('75','0.75') for x in samplelist]
legend_names = [x.replace('snowmass_delphes/output/','').replace('.root','').replace('_0_','').replace('_','').replace('pi',' #pi_{D}').replace('lam',' #Lambda_{D}=').replace('Nc3Nf3','').replace('sFoff','').replace('decay','').replace('probvec',' probvec=').replace('05','0.5 ').replace('75','0.75 ') for x in samplelist]
# legend_names = [x.replace('new_benchmarks/'+R+'/','').replace('.root','').replace('_0_','').replace('_','').replace('pi',' #pi_{D}').replace('lam','#Lambda_{D}= ').replace('Nc3Nf3','').replace('sFoff','').replace('decay',' decay').replace('pp','').replace('R',' R') for x in samplelist]

# legend = root.TLegend(0.4,0.80,0.9,0.9)
legend = root.TLegend(0.3,0.75,0.9,0.9)
legend.SetTextSize(0.055)
legend.SetFillStyle(0)
legend.AddEntry(hist1,legend_names[0],"l")
legend.AddEntry(hist2,legend_names[1],"l")
legend.SetLineWidth(1)
legend.Draw()

canvas.Update()
canvas.Print(savename+"_"+R+".png")
canvas.Print(savename+"_"+R+".pdf")

