R="R04"
echo "Working on $R"
r='0.4'

python3.9 analysis.py snowmass_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay/Nc3Nf3_sFoff_pp_1pi_decay_0.root $R/Nc3Nf3_sFoff_1pi_$R.root $r
python3.9 analysis.py snowmass_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay/Nc3Nf3_sFoff_pp_2pi_decay_0.root $R/Nc3Nf3_sFoff_2pi_$R.root $r
python3.9 analysis.py snowmass_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay/Nc3Nf3_sFoff_pp_3pi_decay_0.root $R/Nc3Nf3_sFoff_3pi_$R.root $r

# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_1pi_decay_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam_5_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_1pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_1pi_decay_lam_50_0_$R.root $r

# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_2pi_decay_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam_5_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_2pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_2pi_decay_lam_50_0_$R.root $r

# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_0.root new_benchmarks/$R/Nc3Nf3_sFoff_3pi_decay_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_lam_5_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam_5_0_$R.root $r
# python3.9 analysis.py new_benchmarks/Nc3Nf3_sFoff_pp_3pi_decay_lam_50_0.root new_benchmarks/$R/Nc3Nf3_sFoff_pp_3pi_decay_lam_50_0_$R.root $r


python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n tau21_j1 -s Plots/$R/tau21_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n tau21_j2 -s Plots/$R/tau21_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n tau32_j1 -s Plots/$R/tau32_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n tau32_j2 -s Plots/$R/tau32_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet_mJJ -s Plots/$R/jet_mJJ_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet_met_rt -s Plots/$R/jet_met_rt_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet1_mass_softdrop -s Plots/$R/jet1_mass_softdrop_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet2_mass_softdrop -s Plots/$R/jet2_mass_softdrop_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n momentgirth_j1 -s Plots/$R/momentgirth_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n momentgirth_j2 -s Plots/$R/momentgirth_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n momenthalf_j1 -s Plots/$R/momenthalf_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n momenthalf_j2 -s Plots/$R/momenthalf_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n multiplicity_j1 -s Plots/$R/multiplicity_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n multiplicity_j2 -s Plots/$R/multiplicity_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n ptd_j1 -s Plots/$R/ptd_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n ptd_j2 -s Plots/$R/ptd_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n majaxis_j1 -s Plots/$R/majaxis_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n majaxis_j2 -s Plots/$R/majaxis_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n minaxis_j1 -s Plots/$R/minaxis_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n minaxis_j2 -s Plots/$R/minaxis_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n avgaxis_j1 -s Plots/$R/avgaxis_j1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n avgaxis_j2 -s Plots/$R/avgaxis_j2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n ptd_j1_calc -s Plots/$R/ptd_j1_calc_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n ptd_j2_calc -s Plots/$R/ptd_j2_calc_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n dPhi1 -s Plots/$R/dPhi1_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n dPhi2 -s Plots/$R/dPhi2_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet_mJJ_softdrop -s Plots/$R/jet_mJJ_softdrop_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n mt_jj -s Plots/$R/mt_jj_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n njets -s Plots/$R/njets_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet1_pt -s Plots/$R/jet1_pt_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n jet2_pt -s Plots/$R/jet2_pt_$R
python3.9 plotter_temp.py -l $R/Nc3Nf3_sFoff_1pi_$R.root $R/Nc3Nf3_sFoff_2pi_$R.root $R/Nc3Nf3_sFoff_3pi_$R.root -n deltaR -s Plots/$R/deltaR_$R

