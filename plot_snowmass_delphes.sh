R="R04"
echo "Working on $R"
r='0.4'

pi1_prob05="snowmass_delphes/output/Nc3Nf3_sFoff_1pi_decay_lam10_probvec_05_$R.root"
pi1_prob75="snowmass_delphes/output/Nc3Nf3_sFoff_1pi_decay_lam10_probvec_75_$R.root"

pi2_prob05="snowmass_delphes/output/Nc3Nf3_sFoff_2pi_decay_lam10_probvec_05_$R.root"
pi2_prob75="snowmass_delphes/output/Nc3Nf3_sFoff_2pi_decay_lam10_probvec_75_$R.root"

pi3_prob05="snowmass_delphes/output/Nc3Nf3_sFoff_3pi_decay_lam10_probvec_05_$R.root"
pi3_prob75="snowmass_delphes/output/Nc3Nf3_sFoff_3pi_decay_lam10_probvec_75_$R.root"


# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_1pi_decay_lam_10_probvec_05_0.root $pi1_prob05 $r
# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_1pi_decay_lam_10_probvec_75_0.root $pi1_prob75 $r

# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_2pi_decay_lam_10_probvec_05_0.root $pi2_prob05 $r
# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_2pi_decay_lam_10_probvec_75_0.root $pi2_prob75 $r

# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_3pi_decay_lam_10_probvec_05_0.root $pi3_prob05 $r
# python3.9 analysis.py snowmass_delphes/root_files/Nc3Nf3_sFoff_pp_3pi_decay_lam_10_probvec_75_0.root $pi3_prob75 $r


# Plotting script
histlist=("njets" "jet1_mass_softdrop" "jet2_mass_softdrop" "momentgirth_j1" "momentgirth_j2" "momentgirth_j_all" "momenthalf_j1" "momenthalf_j2" "momenthalf_j_all" "multiplicity_j1" "multiplicity_j2" "multiplicity_j_all" "ptd_j1" "ptd_j2" "ptd_j_all" "majaxis_j1" "majaxis_j2" "majaxis_j_all" "minaxis_j1" "minaxis_j2" "minaxis_j_all" "avgaxis_j1" "avgaxis_j2" "avgaxis_j_all" "tau21_j1" "tau21_j2" "tau21_j_all" "tau32_j1" "tau32_j2" "tau32_j_all" "lha_j1" "lha_j2" "lha_j_all" "thrust_j1" "thrust_j2" "thrust_j_all" "ptd_j1_calc" "ptd_j2_calc" "ptd_j_all_calc" "deltaR")
pi_list=("1pi" "2pi")
# histlist=("deltaR")

# histlist=("ptd_j1_calc" "ptd_j2_calc" "ptd_j_all_calc")
# histlist=("tau21_j2" "tau21_j1")

save_plots_1pi="snowmass_delphes/Plots/1pi/$R"
save_plots_2pi="snowmass_delphes/Plots/2pi/$R"
save_plots_3pi="snowmass_delphes/Plots/3pi/$R"
compare_prob_05="snowmass_delphes/Plots/compare_prob_05/$R"
compare_prob_75="snowmass_delphes/Plots/compare_prob_75/$R"

for hist in ${histlist[@]}
do
    # python3.9 plotter_temp.py -l $pi1_prob05 $pi2_prob05 $pi3_prob05 -n $hist -s $compare_prob_05/$hist -r $R
    # python3.9 plotter_temp.py -l $pi1_prob75 $pi2_prob75 $pi3_prob75 -n $hist -s $compare_prob_75/$hist -r $R
    # python3.9 plot_overlay_2_files.py -l $pi1_prob05 $pi1_prob75 -n $hist -s $save_plots_1pi/$hist -r $R
    # python3.9 plot_overlay_2_files.py -l $pi2_prob05 $pi2_prob75 -n $hist -s $save_plots_2pi/$hist -r $R
    # python3.9 plot_overlay_2_files.py -l $pi3_prob05 $pi3_prob75 -n $hist -s $save_plots_3pi/$hist -r $R

    python3.9 ratioplot.py -l $pi1_prob75 $pi1_prob05 -n $hist -s $save_plots_1pi/$hist -r $R
    python3.9 ratioplot.py -l $pi2_prob75 $pi2_prob05 -n $hist -s $save_plots_2pi/$hist -r $R
    python3.9 ratioplot.py -l $pi3_prob75 $pi3_prob05 -n $hist -s $save_plots_3pi/$hist -r $R
done











