import sys
import numpy as np
import ROOT as root
from helpers import *

try:
    input = raw_input
except:
    pass

root.gSystem.Load("libDelphes")

try:
    root.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
    root.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
    pass

inputFile = sys.argv[1]
print("Input file :",inputFile)

chain = root.TChain("Delphes")
chain.Add(inputFile)

treeReader = root.ExRootTreeReader(chain)
num_events = treeReader.GetEntries()

branchJet = treeReader.UseBranch("ParticleFlowJet04")
branchParticle = treeReader.UseBranch("Particle")
branchElectron = treeReader.UseBranch("Electron")
branchPhoton = treeReader.UseBranch("Photon")
branchMuon = treeReader.UseBranch("Muon")
branchTower = treeReader.UseBranch("Tower")
branchTrack = treeReader.UseBranch("Track")
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")
branchMissingET = treeReader.UseBranch("MissingET")
branchScalarHI = treeReader.UseBranch("ScalarHT")

for entry in range(0, 30):
    treeReader.ReadEntry(entry)
    print("The event number is  : {}".format(entry))
    for i in range(0,branchJet.GetEntries()):
        jet = branchJet.At(i)
        print("\t Jet info -- PT = {}, Eta = {}, Phi = {}, num of constituents = {}".format(jet.PT,jet.Eta,jet.Phi,jet.Constituents.GetEntries()))
        for particle in jet.Constituents:
        # for j in range(0,jet.Constituents.GetEntries()):
            # particle = jet.Constituents.At(j)    
            # print(particle)
            # if particle == 0: 
            #     print(" \t \t \t \t None")
            #     continue
            dphi = GetdPhi(particle.Phi, jet.Phi)
            deta = GetdEta(particle.Eta, jet.Eta)
            dr = GetdR(deta, dphi)
            print("\t\t Particle info -- Eta = {}, Phi = {}, dR = {}".format(particle.Eta,particle.Phi,dr))
            



