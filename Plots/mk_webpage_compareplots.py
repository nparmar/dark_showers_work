#!/usr/bin/python3
#
# Give several directories as input, all of them containing the same filenames for the plots (in pdf, png, etc...), 
# this script will create an html table with a row for each filename and a column for each directory so that they can be compared side by side
# If the names of the files between directories are different, this script will sort them alphabetically and plot them in the same order from each directory. 

# This script can be called from anywhere, for example:
# >  ~/core/public_html/mk_webpage_compareplots.py dir1 dir2 dir3
# Will produce an index.html file in the current directory (above dir1) that contains the comparison table
# Any existing index.html file will be removed first!! 
# It will also automatically convert any pdf or eps files into png, using pdftoppm
# 
import sys
import os
from time import localtime, strftime
    
def convert_files_to_png(dir_name):
    if not os.path.exists (dir_name):
        print("This directory does NOT exist! ",dir_name)
        return
        
    files = sorted(os.listdir(dir_name)) # sort it alphabetically
    pngfiles=[]
    for f in files:
        # convert eps to png:
        if (".eps" in f) or (".pdf" in f):
            if (".eps" in f): pngf = f.replace('.eps','.png')
            if (".pdf" in f): pngf = f.replace('.pdf','.png')
            if pngf in files: # The png version might already be there, so skip conversion
                print("png file already there! %s/%s"%(dir_name,pngf))
                continue
            #sam2p_command = "sam2p %s/%s %s/%s" % (dir_name,f,dir_name,pngf)
            fnoext=os.path.splitext(f)[0]
            sam2p_command = "pdftoppm -png -cropbox -r 300 %s/%s %s/%s" % (dir_name,f,dir_name,fnoext)
            print(sam2p_command)
            os.system(sam2p_command)
            # For some reason the pdftoppm adds a "-1" to the fnoext, no matter what. So we need to rename it:
            os.system("mv %s/%s-1.png %s/%s"%(dir_name,fnoext,dir_name,pngf))
            pngfiles.append(pngf)
    
    if (not pngfiles): # if the png files were already there
        pngfiles=[p for p in files if "png" in p] # only add the pngs, not the pdfs
    
    return pngfiles

dirs=sys.argv[1:]
htmlfilename = "index.html"
imgwidth=["800","800","800","800","500","500","500"] # make the plots smaller as we increase the number of directories (columns)
# Make sure to remove any index.html files in that directory:
os.system("rm -f %s" % htmlfilename)
htmlFile = open(htmlfilename, "w")
htmlFile.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n")
htmlFile.write("<html>\n")
htmlFile.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.pas.rochester.edu/~aran/mystyle.css\">\n")
htmlFile.write("<head><title> Comparison </title></head>\n" )
htmlFile.write("<h2><center> Comparing plots in %s </center></h2>\n" % ', '.join(dirs))
htmlFile.write("<body><hr style=\"width: 100%; height: 2px;\">\n")
htmlFile.write("<table border = 5 align = center>\n")

# Print first row with names of dirs (header of table):
htmlFile.write("<tr>\n")
for d in dirs:
    htmlFile.write('    <th>%s</th>\n'%d)
htmlFile.write("</tr>\n")

filesperdir=[] # 2D list containing for each directory the names of the png files inside
# This allows to have files with different names in each directory
# For example: R08/avgaxis_j1_R08.pdf and R10/avgaxis_j1_R10.pdf
for d in dirs:
    fs=convert_files_to_png(d) # fs are the png filenames inside each directory
    filesperdir.append(fs) 

for fcount, f_name in enumerate(fs): # loop over files
    htmlFile.write("<tr>\n")
    for dcount,dir_name in enumerate(dirs): # loop over dirs
        htmlFile.write('    <td><img src="%s/%s" alt="" width=%s></img>\n    <br>%s</td>\n'%(dir_name,filesperdir[dcount][fcount],imgwidth[len(dirs)],filesperdir[dcount][fcount]))
    htmlFile.write("</tr>\n")
    
# Close file:   
htmlFile.write("</table>\n") 
htmlFile.write("<hr style=\"width: 100%; height: 2px;\">\n")
htmlFile.write("<address><a href=\"mailto:aran@mail.cern.ch\">Aran Garcia-Bellido</a></address>\n")
date = strftime("%a, %d %b %Y %H:%M:%S", localtime())
htmlFile.write("<!-- hhmts start --><span style=\"color: rgb(0, 0, 0);\"> Last modified: %s </span><!-- hhmts end -->\n" % date) 
htmlFile.write("</body>\n</html>\n")

print(dirs)
print("Done! Check the webpage with all your plots in %s " % htmlfilename)

