from ast import Add
from matplotlib.pyplot import hist
import numpy as np
import ROOT as root
import argparse

def AddOverflowBin(hist):
    entries = hist.GetEntries() # making sure the entries are counted
    numbins = hist.GetNbinsX() # Get the number of bins get the bin contents of the overflow bin
    hicontent = hist.GetBinContent(numbins) # Information necessary so that the last overflow bin contents can be added to the last bin
    overflow = hist.GetBinContent(numbins+1) # Getting the overflow bin content
    locontent = hist.GetBinContent(1) # Not useful here but if you need to also get the underflow printed
    underflow = hist.GetBinContent(0) # Getting the underflow bin content 
    if overflow > 0:
        hist.SetBinContent(numbins,hicontent+overflow) # 
        hist.SetEntries(entries) # restore the number of entries.
    
    # if underflow > 0:
    #     hist.SetBinContent(1,locontent+underflow)
    #     hist.SetEntries(entries)
    return hist

parser = argparse.ArgumentParser()
parser.add_argument('-l','--list', nargs='+', dest='rootfiles',help='root files', default=None, type=str )
parser.add_argument('-n','--histname', dest='histname',help='histogram',default=None,type=str)
parser.add_argument('-s','--save',dest='savename',help='save fig name',default=None,type=str)
parser.add_argument('-r','--radius', dest='R',help='radius of the Jet', default=None,type=str)

args = parser.parse_args()
samplelist = args.rootfiles
histname = args.histname
savename = args.savename
R = args.R

rootfile1 = root.TFile(samplelist[0])
rootfile2 = root.TFile(samplelist[1])

print("Input files are - {} \n {}".format(samplelist[0],samplelist[1]))

hist1 = rootfile1.Get(histname)
hist2 = rootfile2.Get(histname)



canvas = root.TCanvas("c","canvas",800,800)
pad1 = root.TPad("pad1","pad1",0,0.3,1,1.0)
pad1.SetBottomMargin(0)
pad1.SetLeftMargin(0.165)
pad1.SetRightMargin(0.06)
pad1.SetGridx()
pad1.SetLogy()
pad1.Draw()
pad1.cd()

hist1.SetStats(0)
hist2.SetStats(0)
hist1.SetTitle("")
hist1.Draw("hist")
hist2.Draw("hist sames")
hist1.GetYaxis().SetTitle("Events")

# To avoid the 0 to be clipped off
# hist1.GetYaxis().SetLabelSize(0.)
# axis = root.TGaxis(-5,20,-5,220,20,220,510," ")
# axis.SetLabelFont(43)
# axis.SetLabelSize(15)
# axis.Draw()


# Legends

# legend_names = [x.replace('snowmass_delphes/output/','').replace('.root','').replace('_0_',' ').replace('_',' ').replace('pi','#pi').replace('lam','#Lambda= ').replace('Nc3Nf3','').replace('sFoff','').replace('decay','').replace('probvec','probvec=').replace('05','0.5').replace('75','0.75') for x in samplelist]
# legend_names = [x.replace('snowmass_delphes/output/','').replace('.root','').replace('_0_','').replace('_','').replace('pi',' #pi_{D}').replace('lam',' #Lambda_{D}=').replace('Nc3Nf3','').replace('sFoff','').replace('decay','').replace('probvec',' probvec=').replace('05','0.5 ').replace('75','0.75 ') for x in samplelist]
legend_names = [x.replace('snowmass','').replace('delphes','').replace('/output/','').replace('_pp_','').replace('.root','').replace('_0_','').replace('_','').replace('pi',' #pi_{D}').replace('lam10','').replace('Nc3Nf3','').replace('sFoff','').replace('decay','').replace('probvec','probvec=').replace('05','0.5 ').replace('75','0.75 ').replace('08','0.8').replace('04','0.4') for x in samplelist]
# legend_names = [x.replace('new_benchmarks/'+R+'/','').replace('.root','').replace('_0_','').replace('_','').replace('pi',' #pi_{D}').replace('lam','#Lambda_{D}= ').replace('Nc3Nf3','').replace('sFoff','').replace('decay',' decay').replace('pp','').replace('R',' R') for x in samplelist]

legend = root.TLegend(0.38,0.75,0.93,0.9)
# legend = root.TLegend(0.2,0.70,0.9,0.9)
# legend.SetTextSize(0.055)
legend.SetTextSize(0.062)
# legend.SetNColumns(2)
legend.SetFillStyle(0)
legend.AddEntry(hist1,legend_names[0],"l")
legend.AddEntry(hist2,legend_names[1],"l")
legend.SetLineWidth(1)
legend.Draw()


canvas.cd()
pad2 = root.TPad("pad2","pad2",0,0.05,1,0.3)
pad2.SetTopMargin(0)
pad2.SetLeftMargin(0.165)
pad2.SetBottomMargin(0.5)
pad2.SetRightMargin(0.06)
pad2.SetGridx()
pad2.Draw()
pad2.cd()

ratiohist = hist1.Clone("ratiohist")
ratiohist.SetLineColor(1)
ratiohist.SetMinimum(0.8)
ratiohist.SetMaximum(1.35)
ratiohist.SetStats(0)
ratiohist.Divide(hist2)
ratiohist.SetMarkerStyle(21)
ratiohist.Draw("ep")

hist1.SetLineColor(8)
hist1.SetLineWidth(2)
hist1.SetLineStyle(1)
hist1.GetYaxis().SetTitleSize(40)
# hist1.GetYaxis().SetLabelSize(0.065)
hist1.GetYaxis().SetTitleFont(43)
hist1.GetYaxis().SetTitleOffset(1.7)
hist1.GetYaxis().SetTitle("Events")
hist1.GetYaxis().SetLabelFont(43)
hist1.GetYaxis().SetLabelSize(35)

hist2.SetLineColor(9)
hist2.SetLineWidth(2)
hist2.SetLineStyle(5)

ratiohist.SetTitle("")

ratiohist.GetYaxis().SetTitle("ratio")
ratiohist.GetYaxis().SetTitleSize(40)
ratiohist.GetYaxis().SetTitleFont(43)
ratiohist.GetYaxis().SetTitleOffset(1.5)
ratiohist.GetYaxis().SetLabelFont(43)
ratiohist.GetYaxis().SetLabelSize(25)

ratiohist.GetXaxis().SetTitleSize(40)
ratiohist.GetXaxis().SetTitleFont(43)
ratiohist.GetXaxis().SetTitleOffset(1)
ratiohist.GetXaxis().SetLabelFont(43)
ratiohist.GetXaxis().SetLabelSize(35)


canvas.Print(savename+"_"+R+".pdf")
canvas.Print(savename+"_"+R+".png")

